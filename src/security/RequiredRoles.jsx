import React, { Component } from 'react';
import { connect } from 'react-redux';

@connect(s => s)
class RequiredRoles extends Component {
    render() {
        const userRoles = (this.props.user.roles || []);

        if (!this.props.anyRoles)
            return this.props.children;

        if (this.props.anyRoles.some(role => userRoles.indexOf(role) !== -1))
            return this.props.children;

        return null;
    }
}

export default RequiredRoles;