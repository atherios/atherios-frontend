import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import Provider from 'react-redux/lib/components/Provider';
import UserDashboardStore from './UserDashboardStore';
import UserDashboard from './UserDashboard';

class UserDashboardPage extends Component {

    render() {
        return (
            <Provider store={UserDashboardStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>
                        <UserDashboard />
                    </div>
                </AtheriosLayout>
            </Provider>
        );
    }


}

export default UserDashboardPage;
