import React, { Component } from 'react';
import Card from 'material-ui/Card/Card';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import { connect } from 'react-redux';
import moment from 'moment';
import Avatar from 'material-ui/Avatar/Avatar';
import { white, pink500, grey600, blue500 } from 'material-ui/styles/colors';
import Link from 'react-router-dom/Link';
import IconButton from 'material-ui/IconButton/IconButton';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import Chip from 'material-ui/Chip/Chip';
import UserDashboardService from './UserDashboardService';

@connect(s => s)
class UserProposalsCard extends Component {
    render() {
        return (
            <Card>
                <Subheader>Moje wnioski</Subheader>
                {this.props.proposals.map(proposal => [
                    <Divider />,
                    <div >

                        <div key={proposal.id} className="row space--between" style={{ alignItems: 'center' }}  >
                            <div className="row" style={{ alignItems: 'center' }}>
                                <div>
                                    <Link to={`/proposals/${proposal.id}`}>
                                        <IconButton>
                                            <RemoveRedEye color={blue500} />
                                        </IconButton>
                                    </Link>
                                </div>
                                <div style={{ padding: 20 }}>
                                    <span>Wniosek <b>#{proposal.id}</b> z kategorii {this.findProposalCategory(proposal).summary}.</span>
                                </div>
                                <Chip>{this.findState(proposal.category_id, proposal.state_id).label}</Chip>
                            </div>
                            <div style={{ width: 180 }}>

                                <span style={{ color: grey600 }}>{this.formatDate(proposal.created_date)}</span>
                            </div>
                        </div>
                    </div>
                ])}
            </Card>
        );
    }

    findProposalCategory(proposal) {
        return this.props.proposalCategories.find(category => category.id === proposal.category_id) || {};
    }

    findState = (categoryId, stateId) => {
        const categoryStates = this.props.categoryStates[categoryId];

        if (categoryStates)
            return categoryStates.find(state => state.id === stateId);

        UserDashboardService.fetchProposalCategoryStates(categoryId);
        return {
            label: 'Ładowanie...'
        }
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}

export default UserProposalsCard;