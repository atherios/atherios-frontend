import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    proposalCategories: [],
    notifications: [],
    proposals: [],
    categoryStates: {}
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'PROPOSAL_CATEGORIES_FETCHED':
            return Object.assign({}, state, {
                proposalCategories: action.payload
            })
        case 'NOTIFICATIONS_FETCHED': {
            return Object.assign({}, state, {
                notifications: action.payload
            });
        }
        case 'PROPOSALS_FETCHED': {
            return Object.assign({}, state, {
                proposals: action.payload
            });
        }
        case 'PROPOSAL_CATEGORY_STATES_FETCHED':
            return Object.assign({}, state, {
                categoryStates: Object.assign({}, state.categoryStates, action.payload)
            });
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;