import UserDashboardStore from "./UserDashboardStore";
import Requests from "../http/Requests";

class UserDashboardService {


    static async fetchProposalCategories() {
        const proposalCategories = await Requests.jsonGet('/api/proposal-categories');
        UserDashboardStore.dispatch({
            type: 'PROPOSAL_CATEGORIES_FETCHED',
            payload: proposalCategories
        });
    }

    static async fetchProposals() {
        const proposals = await Requests.jsonGet('/api/users/current/proposals');
        UserDashboardStore.dispatch({
            type: 'PROPOSALS_FETCHED',
            payload: proposals
        });
    }

    static async fetchProposalCategoryStates(categoryId) {
        const states = await Requests.jsonGet(`/api/proposal-categories/${categoryId}/states`);
        const payload = {};
        payload[categoryId] = states;
        UserDashboardStore.dispatch({
            type: 'PROPOSAL_CATEGORY_STATES_FETCHED',
            payload
        });
    }

    static async fetchNotifcations() {
        const proposals = await Requests.jsonGet('/api/users/current/notifications');
        UserDashboardStore.dispatch({
            type: 'NOTIFICATIONS_FETCHED',
            payload: proposals
        });
    }

}

export default UserDashboardService;