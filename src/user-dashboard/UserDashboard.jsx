import React, { Component } from 'react';
import UserDashboardService from './UserDashboardService';
import { Card, CardTitle } from 'material-ui/Card';
import { connect } from 'react-redux';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import UserNotificationCard from './UserNotificationCard';
import UserProposalsCard from './UserProposalsCard';

@connect(s => s)
class UserDashboard extends Component {
    render() {
        return (
            <div>
                <div className="row space--between">
                    <div style={{ width: '49.5%' }}>
                        <Card>
                            <Subheader>Powiadomienia</Subheader>
                            <Divider />
                        </Card>

                        {this.props.notifications.map((notification, i) =>
                            [<UserNotificationCard notification={notification} key={i} />, <br />]
                        )}
                    </div>
                    <div style={{ width: '49.5%' }}>
                        <UserProposalsCard />
                    </div>
                </div>
            </div>
        );
    }

    componentWillMount() {
        UserDashboardService.fetchProposalCategories();
        UserDashboardService.fetchNotifcations()
        UserDashboardService.fetchProposals();
    }
}

export default UserDashboard;