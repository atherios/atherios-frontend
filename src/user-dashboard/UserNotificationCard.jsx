import React, { Component } from 'react';
import CardTitle from 'material-ui/Card/CardTitle';
import PriorityAvatar from '../notifications/PriorityAvatar'
import { priorities } from '../notifications/NotificationPriorities'
import { Card, CardText } from 'material-ui/Card';
import { grey600 } from 'material-ui/styles/colors';
import moment from 'moment';
import showdown from 'showdown';
import Divider from 'material-ui/Divider/Divider';

const converter = new showdown.Converter();
class UserNotificationCard extends Component {
    render() {
        const { notification } = this.props;
        return (
            <Card>
                <div style={{ height: 5, backgroundColor: priorities[notification.notification.priority].color }}></div>
                <CardTitle>
                    <div className="row space--between" style={{ alignItems: 'center' }}>
                        <div className="row" style={{ alignItems: 'center' }}>
                            <PriorityAvatar priority={priorities[notification.notification.priority]} />
                            <span style={{ fontSize: '1.3em' }}>{notification.notification.title}</span>
                        </div>
                        <div style={{ width: 180 }}>
                            <span style={{ color: grey600 }}>{this.formatDate(notification.time)}</span>
                        </div>
                    </div>
                </CardTitle>
                <Divider />
                <CardTitle>
                    <span style={{ color: grey600 }}>Od </span><b>@{notification.author.username}</b>
                </CardTitle>
                <Divider />
                <CardText>
                    <div dangerouslySetInnerHTML={{ __html: converter.makeHtml(notification.notification.content) }}>
                    </div>
                </CardText>
                <div style={{ height: 5, backgroundColor: priorities[notification.notification.priority].color }}></div>
            </Card >
        );
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}

export default UserNotificationCard;