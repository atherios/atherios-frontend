import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    templates: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'CONTRACT_TEMPLATES_FETCHED':
            return Object.assign({}, state, {
                templates: action.payload
            });

    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;