import React, { Component } from 'react';
import { connect } from 'react-redux';
import AtheriosLayout from '../layout/AtheriosLayout';
import ContractTemplatesService from './ContractTemplatesService'
import ContractTemplatesHeader from './ContractTemplatesHeader';

@connect(s => s)
class ContractTemplates extends Component {
    render() {
        return (
            <AtheriosLayout>
                
                <ContractTemplatesHeader />
            </AtheriosLayout>
        );
    }

    componentDidMount() {
        ContractTemplatesService.fetchContractTemplates();
    }
}

export default ContractTemplates;