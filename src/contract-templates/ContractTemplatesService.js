import ContractTemplatesStore from './ContractTemplatesStore';
import Requests from '../http/Requests';


export default class ContractTemplatesService {

    static async fetchContractTemplates() {
        const templates  = await Requests.jsonGet('/api/contract-templates');
        ContractTemplatesStore.dispatch({
            type: 'CONTRACT_TEMPLATES_FETCHED',
            payload: templates
        })
    }

    static async deleteTemplate(templateId) {
        return Requests.jsonDelete(`/api/contract-templates/${templateId}`);
    }

    static async createContractTemplate(fileId) {
        return Requests.jsonPost(`/api/contract-templates`, {
            file_id: fileId
        });
    }

}