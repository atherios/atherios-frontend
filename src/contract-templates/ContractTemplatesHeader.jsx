import React, { Component } from 'react';
import { Card, CardTitle } from 'material-ui/Card';
import Divider from 'material-ui/Divider/Divider';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import Requests from '../http/Requests';
import ContractTemplatesService from './ContractTemplatesService';
import { connect } from 'react-redux';
import ContracTemplatesCard from './ContracTemplatesCard';

@connect(s => s)
class ContractTemplatesHeader extends Component {

    render() {
        const { templates } = this.props;
        return (
            <Card>
                <CardTitle>Szablony umów rekrutacyjnych</CardTitle>
                <Divider />
                <div style={{ padding: 15 }}>
                    <div>
                        <RaisedButton secondary={true} label="Dodaj nowy szablon" onClick={this.openUploadWindow} />
                        <input accept=".jrxml" id="contract-template-upload" type="file" onChange={this.uploadContractTemplate} className="hidden" />
                    </div>
                    <div className="row">
                        {templates.map(template => <ContracTemplatesCard key={template.id} template={template} />)}
                    </div>
                </div>

            </Card>
        );
    }

    uploadContractTemplate = async (e) => {
        const files = e.target.files;
        if (files.length === 0)
            return;
        const file = files[0];

        const reader = new FileReader();
        const form_data = new FormData();

        form_data.append('file', file);

        const response = await Requests.postFile('/api/files', form_data);
        const fileJson = await response.json();

        await ContractTemplatesService.createContractTemplate(fileJson.file_id);
        ContractTemplatesService.fetchContractTemplates();
    }

    openUploadWindow = () => {
        document.getElementById('contract-template-upload').click()
    }
}

export default ContractTemplatesHeader;