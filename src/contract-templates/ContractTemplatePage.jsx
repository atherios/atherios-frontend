import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ContractTemplatesStore from './ContractTemplatesStore'
import ContractTemplates from './ContractTemplates';

class ContractTemplatePage extends Component {
    render() {
        return (
            <Provider store={ContractTemplatesStore}>
                <ContractTemplates />
            </Provider>
        );
    }
}

export default ContractTemplatePage;