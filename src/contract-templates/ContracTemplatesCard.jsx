import React, { Component } from 'react';
import FileCard from '../proposal-categories/settings/attachments/FileCard';
import apiOrigin from 'apiOrigin';
import ContractTemplates from './ContractTemplates';
import ContractTemplatesService from './ContractTemplatesService';
import Dialog from 'material-ui/Dialog/Dialog';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Requests from '../http/Requests';
import Divider from 'material-ui/Divider/Divider';

class ContracTemplatesCard extends Component {

    state = {
        dialogOpen: false,
        usages: []
    }

    render() {
        const { template } = this.props;
        const templateUsages = this.state.usages;

        return (
            <div>
                <FileCard
                    showDeleteButton={true}
                    onDelete={this.openDialog}
                    link={apiOrigin + '/api' + template.link}
                    fileName={template.file_name}
                    size={template.size} />
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: true })}>
                    {
                        templateUsages.length > 0 ?
                            <div>
                                Nie możesz skasować szablonu <b>{template.file_name}</b>.
                                <br /><br />
                                <Divider />
                                <br />
                                Jest używany w poniższych stanach rekrutacji:
                                <ul>
                                    {templateUsages.map(usage => <li key={usage.state.id}>{usage.state.label}</li>)}
                                </ul>
                            </div> :
                            <div>
                                Czy na pewno chcesz skasować szablon <b>{template.file_name}</b>?
                            </div>
                    }
                    <br />
                    <div className="row space--between">
                        <FlatButton onClick={() => this.setState({ dialogOpen: false })} label="Zamknij" secondary={true} />
                        <FlatButton disabled={templateUsages.length > 0} label="Skasuj" onClick={this.deleteTemplate} secondary={true} />
                    </div>
                </Dialog>
            </div>
        );
    }

    deleteTemplate = async () => {
        const { template } = this.props;
        
        this.setState({dialogOpen: false})
        await ContractTemplatesService.deleteTemplate(template.id);
        ContractTemplatesService.fetchContractTemplates();
    }

    openDialog = async () => {
        this.setState({ dialogOpen: true });
        const { template } = this.props;
        const usages = await Requests.jsonGet(`/api/contract-templates/${template.id}/usages`);
        this.setState({
            usages
        })
    }
}

export default ContracTemplatesCard;