import React, { Component } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Divider from 'material-ui/Divider/Divider';
import NotificationForm from './NotificationForm';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import NotificationsService from './NotificationsService';
import Dialog from 'material-ui/Dialog/Dialog';
import Link from 'react-router-dom/Link';

class AddNotification extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <Card>
                    <CardTitle>
                        <div className="row space--between" style={{ width: '100%', alignItems: 'center' }}>
                            <span style={{ fontSize: '1.6em' }}>Dodajesz nowe powiadomienie</span>
                            <RaisedButton onClick={this.submitForm} secondary={true} label="Zapisz" />
                        </div>
                    </CardTitle>
                </Card>
                <br />
                <NotificationForm onFormSubmitted={this.createNotification} />
                <Dialog open={this.state.dialogOpen} modal={true}>
                    <div>
                        Powiadomienie zostało zapisane. Możesz je obejrzeć <Link to="/notifications">tutaj</Link>.
                    </div>
                </Dialog>
            </div>
        );
    }
    submitForm = () => {
        document.getElementById('notification-form__submit').click();
    }

    createNotification = async (notification) => {
        await NotificationsService.createNotification(notification);
        this.setState({ dialogOpen: true })
    }
}

export default AddNotification;