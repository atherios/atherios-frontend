import React, { Component } from 'react'
import { connect } from 'react-redux';
import NotificationsService from './NotificationsService';
import { Card, CardTitle } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import NotificationForm from './NotificationForm';
import Dialog from 'material-ui/Dialog/Dialog';
import Link from 'react-router-dom/Link';

@connect(s => s)
export default class EditNotification extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        const { notification, notificationId } = this.props;
        return (
            <div>
                <Card>
                    <CardTitle>
                        <div className="row space--between" style={{ width: '100%', alignItems: 'center' }}>
                            <span style={{ fontSize: '1.6em' }}>Edytujesz powiadomienie <b>{notification.title}</b></span>
                            <RaisedButton onClick={this.submitForm} secondary={true} label="Zapisz" />
                        </div>
                    </CardTitle>
                </Card>
                <br />
                {notification.id === notificationId ? <NotificationForm initialState={notification} onFormSubmitted={this.saveNotification} /> : null}
                <Dialog open={this.state.dialogOpen} modal={true}>
                    <div>
                        Powiadomienie zostało zapisane. Możesz je obejrzeć <Link to="/notifications">tutaj</Link>.
                    </div>
                </Dialog>
            </div>
        )
    }

    submitForm = () => {
        document.getElementById('notification-form__submit').click();
    }

    saveNotification = async (notification) => {
        const { notificationId } = this.props;
        await NotificationsService.saveNotification(notificationId, notification)
        this.setState({ dialogOpen: true })
    }

    componentWillMount() {
        const { notificationId } = this.props;
        NotificationsService.fetchNotification(notificationId);
    }

    componentWillUnmount() {
        this.props.dispatch({ type: 'RESET_NOTIFICATION_FORM' })
    }


}
