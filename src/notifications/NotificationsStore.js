import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    notifications: [],
    notification: {
        author: {}
    },
    distribution: [],
    users: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'NOTIFICATIONS_FETCHED': {
            return Object.assign({}, state, {
                notifications: action.payload
            });
        }
        case 'NOTIFICATION_FETCHED': {
            return Object.assign({}, state, {
                notification: action.payload
            });
        }
        case 'RESET_NOTIFICATION_FORM': {
            return Object.assign({}, state, {
                notification: {
                    author: {}
                }
            });
        }
        case 'NOTIFICATION_DISTRIBUTION_FETCHED': {
            return Object.assign({}, state, {
                distribution: action.payload
            });
        }
        case 'USERS_FETCHED': {
            return Object.assign({}, state, {
                users: action.payload
            });
        }
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;