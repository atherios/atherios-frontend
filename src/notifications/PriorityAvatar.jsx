import React, { Component } from 'react'
import Avatar from 'material-ui/Avatar/Avatar';

export default class PriorityAvatar extends Component {
    render() {
        return (
            <Avatar
                style={{ marginRight: 15 }}
                size={18}
                backgroundColor={this.props.priority.color}
            />
        )
    }
}
