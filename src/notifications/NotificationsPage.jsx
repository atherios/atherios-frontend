import React, { Component } from 'react';
import { Provider } from 'react-redux';
import NotificationsStore from './NotificationsStore';
import Notifications from './Notifications';

class NotificationsPage extends Component {
    render() {
        return (
            <Provider store={NotificationsStore}>
                <Notifications />
            </Provider>
        );
    }
}

export default NotificationsPage;