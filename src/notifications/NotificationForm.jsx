import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import Subheader from 'material-ui/Subheader/Subheader';
import showdown from 'showdown';
import Divider from 'material-ui/Divider/Divider';
import { CardText } from 'material-ui/Card';
import Card from 'material-ui/Card/Card';
import NotificationsService from './NotificationsService';
import SelectField from 'material-ui/SelectField/SelectField';
import MenuItem from 'material-ui/MenuItem/MenuItem';
import Avatar from 'material-ui/Avatar/Avatar';
import { priorities } from './NotificationPriorities';
import PriorityAvatar from './PriorityAvatar';

const converter = new showdown.Converter();
class NotificationForm extends Component {


    constructor(props) {
        super(props)
        this.state = Object.assign({
            title: '',
            content: '',
            priority: 1,
            tags: [],
            errors: {}
        }, props.initialState)
    }

    render() {

        return (
            <form id="notification-form" onSubmit={this.submitForm}>
                <Card>
                    <Subheader>Tytuł powiadomienia</Subheader>
                    <Divider />
                    <CardText>
                        <TextField
                            value={this.state.title}
                            fullWidth={true}
                            underlineShow={false}
                            floatingLabelFixed={true}
                            hintText="Wpisz tytuł"
                            errorText={this.state.errors.title}
                            inputStyle={{ fontSize: '1.8em', lineHeight: '38px' }}
                            onChange={(e, value) => this.setState({ title: value })} />
                    </CardText>
                    <Divider />
                    <Subheader>Priorytet komunikatu</Subheader>
                    <Divider />
                    <CardText>
                        <SelectField
                            hintText="Wybierz priorytet komunikatu"
                            value={this.state.priority}
                            onChange={(e, i, value) => this.setState({ priority: value })}>
                            {
                                [0, 1, 2, 3].map(i => <MenuItem
                                    key={i}
                                    primaryText={this.priorityListItem(priorities[i])}
                                    value={i}
                                />)
                            }
                        </SelectField>
                    </CardText>
                </Card>
                <br />
                <Card>
                    <Subheader>Treść powiadomienia</Subheader>
                    <Divider />
                    <CardText>
                        <TextField
                            value={this.state.content}
                            fullWidth={true}
                            underlineShow={false}
                            floatingLabelFixed={true}
                            multiLine={true}
                            hintText="Wpisz treść"
                            errorText={this.state.errors.content}
                            onChange={(e, value) => this.setState({ content: value })} />

                    </CardText>
                </Card>
                <br />
                <Card>
                    <Subheader>Podgląd</Subheader>
                    <Divider />
                    <CardText>
                        <h1>{this.state.title}</h1>
                        <div dangerouslySetInnerHTML={{ __html: converter.makeHtml(this.state.content) }}>
                        </div>
                    </CardText>
                </Card>

                <input id="notification-form__submit" className="hidden" type="submit" />
            </form>
        );
    }

    priorityListItem = (priority) => {
        return (
            <div className="row" style={{ alignItems: 'center' }}>
                <PriorityAvatar priority={priority} />
                <span>
                    {priority.name}
                </span>
            </div>
        )
    }

    submitForm = async (e) => {
        e.preventDefault();

        if (this.validate()) {
            const notification = {
                title: this.state.title,
                content: this.state.content,
                priority: this.state.priority
            }
            this.props.onFormSubmitted(notification);
        }

    }

    validate = () => {
        const errors = {};

        const titleLength = (this.state.title || '').length;
        if (titleLength < 5)
            errors.title = 'Tytuł powiadomienia powinien mieć przynajmniej pięć znaków';

        const contentLength = (this.state.content || '').length;
        if (contentLength < 5)
            errors.content = 'Treść powiadomienia powinna mieć przynajmniej pięć znaków';

        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default NotificationForm;