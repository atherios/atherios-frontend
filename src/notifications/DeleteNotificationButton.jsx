import React, { Component } from 'react'
import IconButton from 'material-ui/IconButton/IconButton';
import Clear from 'material-ui/svg-icons/content/clear';
import Dialog from 'material-ui/Dialog/Dialog';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import { connect } from 'react-redux';
import NotificationsService from './NotificationsService';

@connect(s => s)
export default class DeleteNotificationButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <IconButton onClick={() => this.setState({ dialogOpen: true })}>
                    <Clear />
                </IconButton>
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: false })}>
                    <div>
                        Czy na pewno chcesz skasować powiadomienie <b>{this.props._notification.title}</b>?
                    </div>
                    <br />
                    <div className="row space--between">
                        <FlatButton secondary={true} label="Zamknij" onClick={() => this.setState({ dialogOpen: false })} />
                        <FlatButton secondary={true} label="Kasuj" onClick={this.deleteNotification} />
                    </div>
                </Dialog>
            </div>
        )
    }

    deleteNotification = async () => {
        const { _notification } = this.props;
        await NotificationsService.deleteNotification(_notification.id);
        this.setState({ dialogOpen: false });
        setTimeout(NotificationsService.fetchNotifications, 200)
    }
}
