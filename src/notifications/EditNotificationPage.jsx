import React, { Component } from 'react'
import NotificationsStore from './NotificationsStore';
import { Provider } from 'react-redux';
import AtheriosLayout from '../layout/AtheriosLayout';
import AddNotification from './AddNotification';
import EditNotification from './EditNotification';

export default class EditNotificationPage extends Component {
    render() {
        return (
            <Provider store={NotificationsStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>
                        <EditNotification notificationId={this.props.match.params.notificationId * 1} />
                    </div>
                </AtheriosLayout>
            </Provider>
        )
    }
}
