import React, { Component } from 'react';
import { Provider } from 'react-redux';
import NotificationsStore from './NotificationsStore';
import AddNotification from './AddNotification';
import AtheriosLayout from '../layout/AtheriosLayout';

class AddNotificationPage extends Component {
    render() {
        return (
            <Provider store={NotificationsStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>
                        <AddNotification />
                    </div>
                </AtheriosLayout>
            </Provider>
        );
    }
}

export default AddNotificationPage;