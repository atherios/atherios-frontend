import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import { Card, CardTitle } from 'material-ui/Card';
import CardText from 'material-ui/Card/CardText';
import Divider from 'material-ui/Divider/Divider';
import { Table, TableHeaderColumn, TableRowColumn } from 'material-ui/Table';
import TableBody from 'material-ui/Table/TableBody';
import TableHeader from 'material-ui/Table/TableHeader';
import TableRow from 'material-ui/Table/TableRow';
import NotificationsService from './NotificationsService';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import Link from 'react-router-dom/Link';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton/IconButton';
import Create from 'material-ui/svg-icons/content/create';
import Clear from 'material-ui/svg-icons/content/clear';
import { priorities } from './NotificationPriorities';
import PrioriotyAvatar from './PriorityAvatar';
import DeleteNotificationButton from './DeleteNotificationButton';

@connect(s => s)
class Notifications extends Component {
    render() {
        const { notifications } = this.props;
        return (
            <AtheriosLayout>
                <div style={{ padding: 20 }}>
                    <Card>
                        <CardTitle>Powiadomienia dla pracowników</CardTitle>
                        <Divider />

                        <CardText>
                            <Link to="/notifications/add-notification">
                                <RaisedButton secondary={true} label="Dodaj powiadomienie" />
                            </Link>
                            <br />
                            <Table selectable={false}>
                                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                                    <TableRow>
                                        <TableHeaderColumn>Tytuł</TableHeaderColumn>
                                        <TableHeaderColumn>Autor</TableHeaderColumn>
                                        <TableHeaderColumn></TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody displayRowCheckbox={false}>
                                    {notifications.map(notification =>
                                        <TableRow key={notification.id}>
                                            <TableRowColumn>
                                                <Link to={`/notifications/${notification.id}/distribution`}>
                                                    <div className="row" style={{ alignItems: 'center' }}>
                                                        <PrioriotyAvatar priority={priorities[notification.priority]} />
                                                        <span>{notification.title}</span>
                                                    </div>
                                                </Link>
                                            </TableRowColumn>
                                            <TableRowColumn>@{notification.author.username}</TableRowColumn>
                                            <TableRowColumn>
                                                <div className="row">
                                                    <Link to={`/notifications/${notification.id}/edit-notification`}>
                                                        <IconButton>
                                                            <Create />
                                                        </IconButton>
                                                    </Link>
                                                    <DeleteNotificationButton _notification={notification} />
                                                </div>
                                            </TableRowColumn>
                                        </TableRow>
                                    )}
                                </TableBody>
                            </Table>
                        </CardText>
                    </Card>
                </div>
            </AtheriosLayout>
        );
    }

    componentWillMount() {
        NotificationsService.fetchNotifications();
    }
}

export default Notifications;