import Requests from "../http/Requests";
import NotificationsStore from "./NotificationsStore";

export default class NotificationsService {

    static async fetchNotifications() {
        const notifications = await Requests.jsonGet('/api/notifications');
        NotificationsStore.dispatch({
            type: 'NOTIFICATIONS_FETCHED',
            payload: notifications
        })
    }

    static async fetchUsers() {
        const users = await Requests.jsonGet(`/api/users`);
        NotificationsStore.dispatch({
            type: 'USERS_FETCHED',
            payload: users
        })
    }

    static async distibuteNotification(notificationId, distribution) {
        return Requests.jsonPost(`/api/notifications/${notificationId}/distribution`, distribution);
    }

    static async fetchNotificationDistributions(notificationId) {
        const distribution = await Requests.jsonGet(`/api/notifications/${notificationId}/distribution`);
        NotificationsStore.dispatch({
            type: 'NOTIFICATION_DISTRIBUTION_FETCHED',
            payload: distribution
        })
    }

    static async deleteNotification(notificationId) {
        return Requests.jsonDelete(`/api/notifications/${notificationId}`);
    }

    static async fetchNotification(notificationId) {
        const notification = await Requests.jsonGet(`/api/notifications/${notificationId}`);
        NotificationsStore.dispatch({
            type: 'NOTIFICATION_FETCHED',
            payload: notification
        })
    }

    static async createNotification(notification) {
        return Requests.jsonPost(`/api/notifications`, notification);
    }

    static async saveNotification(notificationId, notification) {
        return Requests.jsonPut(`/api/notifications/${notificationId}`, notification);
    }


}