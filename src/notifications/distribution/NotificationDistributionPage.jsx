import React, { Component } from 'react'
import {Provider} from 'react-redux';
import NotificationsStore from '../NotificationsStore';
import AtheriosLayout from '../../layout/AtheriosLayout';
import NotificationDistribution from './NotificationDistribution';

export default class NotificationDistributionPage extends Component {
    render() {
        return (
            <Provider store={NotificationsStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>
                        <NotificationDistribution notificationId={this.props.match.params.notificationId * 1} />
                    </div>
                </AtheriosLayout>
            </Provider>
        )
    }
}
