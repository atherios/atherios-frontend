import React, { Component } from 'react';
import { Card, CardText } from 'material-ui/Card';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import DistributeNotificationForm from './DistributeNotificationForm';
import { connect } from 'react-redux';
import NotificationsService from '../NotificationsService';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Dialog from 'material-ui/Dialog/Dialog';

@connect(s => s)
class DistributeNotificationCard extends Component {

    state = {
        dialogOpen: false,
        distribution: {}
    }

    render() {
        const { distribution } = this.state;
        return (
            <Card style={{ width: '49.5%' }}>
                <div className="row space--between" style={{ alignItems: 'center' }}>
                    <Subheader>Wyślij powiadomienie</Subheader>
                    <FlatButton style={{ marginLeft: '-30px' }} secondary={true} onClick={this.submitForm} label="Wyślij powiadomienie" />
                </div>
                <Divider />
                <CardText>
                    <DistributeNotificationForm onFormSubmitted={this.openDistributionDialog} />
                </CardText>
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ open: false })}>
                    <div>
                        <span>Zamierzasz wysłać powiadomienie do </span>
                        {distribution.type === 'FIXED' ? <b>wybranych użytkowników ({distribution.user_ids.length})</b> : null}
                        {distribution.type === 'ALL_USERS' ? <b>wszystkich użytkowników</b> : null}?
                    </div>
                    <br />
                    <div className="row space--between">
                        <FlatButton label="Zamknij" secondary={true} onClick={() => this.setState({ dialogOpen: false })} />
                        <FlatButton label="OK, wysyłamy!" secondary={true} onClick={this.distribute} />
                    </div>
                </Dialog>
            </Card>
        );
    }

    submitForm = () => {
        document.getElementById('notification-distribution-submit').click();
    }

    openDistributionDialog = (distribution) => {
        this.setState({ distribution, dialogOpen: true })
    }

    distribute = async () => {
        const { distribution } = this.state;
        const { notification } = this.props;

        await NotificationsService.distibuteNotification(notification.id, distribution);
        this.setState({ dialogOpen: false })
        NotificationsService.fetchNotificationDistributions(notification.id);
    }

    componentWillMount() {
        NotificationsService.fetchUsers();
    }
}

export default DistributeNotificationCard;