import React, { Component } from 'react'
import NotificationsService from '../NotificationsService';
import { CardTitle, Card } from 'material-ui/Card';
import { connect } from 'react-redux';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import CardText from 'material-ui/Card/CardText';
import DistributeNotificationCard from './DistributeNotificationCard';
import Table from 'material-ui/Table/Table';
import TableHeader from 'material-ui/Table/TableHeader';
import TableRow from 'material-ui/Table/TableRow';
import TableHeaderColumn from 'material-ui/Table/TableHeaderColumn';
import { TableBody, TableRowColumn } from 'material-ui/Table';
import moment from 'moment';

@connect(s => s)
export default class NotificationDistribution extends Component {
    render() {
        const { notification, distribution } = this.props;
        return (
            <div>
                <Card>
                    <CardTitle>
                        <span style={{ fontSize: '1.6em' }}>Dystrybucja komunikatu <b>{notification.title}</b></span>
                    </CardTitle>
                </Card>
                <br />
                <div className="row space--between">
                    <Card style={{ width: '49.5%' }}>
                        <Subheader>Dostarczenia wiadomości</Subheader>
                        <Divider />
                            <Table selectable={false}>
                                <TableHeader style={{padding: 0}} adjustForCheckbox={false} displaySelectAll={false}>
                                    <TableRow>
                                        <TableHeaderColumn>Wysłane przez</TableHeaderColumn>
                                        <TableHeaderColumn>Odbiorca</TableHeaderColumn>
                                        <TableHeaderColumn>Dostarczono</TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody displayRowCheckbox={false}>
                                    {distribution.map((_distribution, i) =>
                                        <TableRow key={i}>
                                            <TableRowColumn>
                                                @{_distribution.author.username}
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                @{_distribution.recipient.username}
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                {this.formatDate(_distribution.time)}
                                            </TableRowColumn>
                                        </TableRow>
                                    )}
                                </TableBody>
                            </Table>
                    </Card>
                    <DistributeNotificationCard />
                </div>
            </div >
        )
    }

    componentWillMount() {
        const { notificationId } = this.props;
        NotificationsService.fetchNotification(notificationId);
        NotificationsService.fetchNotificationDistributions(notificationId);
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}
