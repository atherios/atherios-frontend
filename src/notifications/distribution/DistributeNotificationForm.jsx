import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField/SelectField';
import { MenuItem } from 'material-ui/IconMenu';
import AutoComplete from 'material-ui/AutoComplete/AutoComplete';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip/Chip';

const dataSourceConfig = {
    text: 'username',
    value: 'username',
};

@connect(s => s)
class DistributeNotificationForm extends Component {


    state = {
        type: 'ALL_USERS',
        selectedUsers: [],
        errors: {}
    }

    render() {
        return (
            <form onSubmit={this.onFormSubmitted}>
                <SelectField
                    hintText="Wybierz obszar dystrybucji"
                    fullWidth={true}
                    value={this.state.type}
                    onChange={(e, i, value) => this.setState({ type: value })}>
                    <MenuItem
                        value='ALL_USERS'
                        primaryText="Wszyscy użytkownicy" />
                    <MenuItem
                        value='FIXED'
                        primaryText="Wybrani użytkownicy" />
                </SelectField>
                {
                    this.state.type === 'FIXED' ?
                        <UserSelection
                            errors={this.state.errors}
                            allUsers={this.props.users}
                            selectedUsers={this.state.selectedUsers}
                            onUsersSelected={selectedUsers =>
                                this.setState({
                                    selectedUsers,
                                    errors: Object.assign({}, this.state.errors, { selectedUsers: undefined })
                                })} /> :
                        <br />
                }
                <input type="submit" id="notification-distribution-submit" className="hidden" />
            </form>
        );
    }

    onFormSubmitted = (e) => {
        e.preventDefault();
        

        if (this.validate()) {
            const distribution = {
                type: this.state.type
            }
            if (this.state.type === 'FIXED')
                distribution.user_ids = this.state.selectedUsers.map(user => user.username);
            this.props.onFormSubmitted(distribution);
        }
    }

    validate = () => {
        const errors = {};
        if (this.state.type === 'FIXED') {
            if (this.state.selectedUsers.length === 0)
                errors.selectedUsers = 'Musisz wybrać użytkowników, do których wyślesz powiadomienie';
        }

        this.setState({ errors });
        return Object.keys(errors).length === 0;
    }
}

class UserSelection extends Component {

    state = {
        query: ''
    }

    render() {
        return (
            <div>
                <AutoComplete
                    dataSourceConfig={dataSourceConfig}
                    hintText="Wpisz nazwę użytkownika"
                    searchText={this.state.query}
                    errorText={this.props.errors.selectedUsers}
                    fullWidth={true}
                    onUpdateInput={(value) => this.setState({ query: value })}
                    filter={AutoComplete.fuzzyFilter}
                    onNewRequest={this.addCandidate}
                    dataSource={this.props.allUsers}
                    openOnFocus={true}
                />

                <div className="row" >
                    {
                        this.props.selectedUsers.map(user =>
                            <Chip
                                key={user.username}
                                onRequestDelete={() => this.removeCandidate(user)}
                                style={{ margin: 5 }}>
                                {user.username}
                            </Chip>)
                    }
                </div>
            </div>
        )
    }

    addCandidate = (item, index) => {
        this.setState({ query: '' });

        if (this.props.selectedUsers.some(user => user.username === item.username))
            return;

        const selectedUsers = this.props.selectedUsers.concat([item]);
        this.props.onUsersSelected(selectedUsers);

    }

    removeCandidate = (selectedUser) => {
        const selectedUsers = this.props.selectedUsers
            .filter(user => user.username !== selectedUser.username);
        this.props.onUsersSelected(selectedUsers);
    }
}

export default DistributeNotificationForm;