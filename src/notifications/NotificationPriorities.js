import { blue300, blue100, green500, red600 } from 'material-ui/styles/colors';

export const priorities = [
    {
        name: 'Wiadomość poboczna',
        color: blue100
    },
    {
        name: 'Komunikat',
        color: blue300
    },
    {
        name: 'Ważny komunikat',
        color: green500
    },
    {
        name: 'Wiadomość krytyczna',
        color: red600
    }
];