import React, { Component } from 'react';
import { Provider } from 'react-redux';
import AtheriosLayout from '../layout/AtheriosLayout';
import SearchResultsStore from './SearchResultsStore';
import SearchResults from './SearchResults';

class SearchResultsPage extends Component {
    render() {
        return (
            <Provider store={SearchResultsStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>
                        <SearchResults query={this.props.match.params.query} />
                    </div>
                </AtheriosLayout>
            </Provider>
        );
    }

}

export default SearchResultsPage;