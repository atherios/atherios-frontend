import SearchResultsStore from './SearchResultsStore';
import Requests from '../http/Requests';

class SearchResultsService {
    
    static async fetchHits(query) {
        const hits = await Requests.jsonGet(`/api/search?q=${query}`);
        SearchResultsStore.dispatch({
            type: 'HITS_FETCHED',
            payload: hits
        })
    }
}

export default SearchResultsService;