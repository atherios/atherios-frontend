import React, { Component } from 'react';
import SearchResultsService from './SearchResultsService';
import { CardTitle, Card } from 'material-ui/Card';
import { connect } from 'react-redux';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import CardText from 'material-ui/Card/CardText';
import NotificationHit from './hits/NotificationHit';
import ProposalStateHit from './hits/ProposalStateHit';
import CandidateHit from './hits/CandidateHit';
import ProposalHit from './hits/ProposalHit';
import ProposalCategoryAttachmentHit from './hits/ProposalCategoryAttachmentHit';
import RecruitmentStateHit from './hits/RecruitmentStateHit';
import ProposalCategoryHit from './hits/ProposalCategoryHit';
import Link from 'react-router-dom/Link';


const documentTypes = {
    notification: {
        title: 'Powiadomienie',
        link(payload) {
            return `/notifications/${payload.id}/distribution`
        }
    },
    candidate: {
        title: 'Kandydat',
        link(payload) {
            return `/candidates/${payload.id}`
        }
    },
    'recruitment-state': {
        title: 'Stan rekrutacji',
        link(payload) {
            return `/recruitment/${payload.recruitment_id}/settings`
        }
    },
    'proposal-category': {
        title: 'Kategoria wniosków',
        link(payload) {
            return `/proposal-categories/${payload.id}/board`
        }
    },
    recruitment: {
        title: 'Rekrutacja',
        link(payload) {
            return `/recruitment/${payload.id}`
        }
    },
    'proposal-state': {
        title: 'Stan wniosków',
        link(payload) {
            return `/proposal-categories/${payload.category_id}/settings`
        }
    },
    'proposal-category-attachment': {
        title: 'Materiał dla kategorii wniosków',
        link(payload) {
            return `/proposal-categories/${payload.category_id}/settings`
        }
    },
    proposal: {
        title: 'Wniosek',
        link(payload) {
            return `/proposals/${payload.id}`
        }
    }
}

@connect(s => s)
class SearchResults extends Component {
    render() {
        const { hits, query } = this.props;
        return (
            <div>
                <Card>
                    <CardTitle>Wyniki wyszukiwania dla <b>{query}</b></CardTitle>
                </Card>
                <br />
                {
                    hits.map((hit, i) => [<Link to={documentTypes[hit.document_type].link(hit.payload)}>
                        <Card key={i}>
                            <Subheader>
                                {documentTypes[hit.document_type].title}
                            </Subheader>
                            <Divider />
                            {this.chooseComponent(hit)}
                        </Card>
                    </Link>, <br />])
                }
            </div>
        );
    }

    chooseComponent = (hit) => {
        const { document_type, payload } = hit;
        switch (document_type) {
            case 'notification':
                return <NotificationHit payload={payload} />
            case 'candidate':
                return <CandidateHit payload={payload} />
            case 'recruitment-state':
                return <RecruitmentStateHit payload={payload} />
            case 'proposal-category':
            case 'recruitment':
                return <ProposalCategoryHit payload={payload} />
            case 'proposal-state':
                return <ProposalStateHit payload={payload} />
            case 'proposal-category-attachment':
                return <ProposalCategoryAttachmentHit payload={payload} />
            case 'proposal':
                return <ProposalHit payload={payload} />
            default:
                return <CardText>{document_type}</CardText>
        }
    }

    componentWillReceiveProps(nextProps) {
        const currentQuery = this.props.query
        const nextQuery = nextProps.query

        if (currentQuery !== nextQuery) {
            SearchResultsService.fetchHits(nextQuery)
        }
    }

    componentWillMount() {
        SearchResultsService.fetchHits(this.props.query)
    }
}

export default SearchResults;