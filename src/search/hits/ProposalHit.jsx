import React, { Component } from 'react';
import moment from 'moment';
import Avatar from 'material-ui/Avatar/Avatar';
import { grey100, blue700, grey500, white, pink500 } from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider/Divider';
import { CardText } from 'material-ui/Card';


const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]

class ProposalHit extends Component {
    render() {
        const proposal = this.props.payload;
        return (
            <div>
                <div style={{ padding: 15 }}>
                    <div className="row space--between">
                        <div>
                            <Avatar
                                color={white}
                                backgroundColor={pink500}
                                size={40}
                                style={{ fontSize: '1em', marginRight: 15 }}>#{proposal.id}</Avatar>

                            <span style={{ fontSize: '1.3em' }}>{proposal.user.first_name} {proposal.user.last_name}</span>
                        </div>
                        <span style={{ color: grey500, fontWeight: 500 }}>{this.formatDate(proposal.created_date)}</span>
                    </div>
                </div>
                <Divider />
                <div style={{ padding: 15 }}>
                    <div className="row" style={{ alignItems: 'center' }}>
                        <span>Załączonych plików</span>
                        <Avatar
                            color={white}
                            style={{ marginLeft: 15 }}
                            backgroundColor={pink500}
                            size={25}>{proposal.file_ids.length}</Avatar>
                    </div>
                </div>
            </div>
        );
    }

    computeSize = (size) => {
        let idx = 0;
        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}

export default ProposalHit;