import React, { Component } from 'react';
import CardText from 'material-ui/Card/CardText';
import Avatar from 'material-ui/Avatar/Avatar';
import { grey100, blue700 } from 'material-ui/styles/colors';

class CandidateHit extends Component {
    render() {
        const candidate = this.props.payload;
        return (
            <CardText>
                <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'row' }}>

                    <Avatar
                        color={grey100}
                        backgroundColor={blue700}
                        size={35}
                        style={{ fontSize: '1em', marginRight: 20 }}>
                        {(candidate.first_name || 'A').substring(0, 1)}{(candidate.last_name || 'A').substring(0, 1)}
                    </Avatar>
                    <span style={{ fontSize: '1.5em', display: 'block' }}>
                        {candidate.first_name} {candidate.last_name}
                    </span>
                </div>
            </CardText>
        );
    }
}

export default CandidateHit;