import React, { Component } from 'react';
import Chip from 'material-ui/Chip/Chip';
import { CardText } from 'material-ui/Card';

class ProposalStateHit extends Component {
    render() {
        const { payload } = this.props;
        return (
            <CardText>
                <Chip>{payload.label}</Chip>
            </CardText>
        );
    }
}

export default ProposalStateHit;