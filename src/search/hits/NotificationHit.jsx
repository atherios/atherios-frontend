import React, { Component } from 'react';
import CardTitle from 'material-ui/Card/CardTitle';
import PriorityAvatar from '../../notifications/PriorityAvatar'
import { priorities } from '../../notifications/NotificationPriorities'
import { Card, CardText } from 'material-ui/Card';
import { grey600 } from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider/Divider';

class NotificationHit extends Component {
    render() {
        const notification = this.props.payload;
        return (
            <div>
                <CardTitle>
                    <div className="row" style={{ alignItems: 'center', width: 600 }}>
                        <div className="row" style={{ alignItems: 'center' }}>
                            <PriorityAvatar priority={priorities[notification.priority]} />
                            <span style={{ fontSize: '1.3em' }}>{notification.title}</span>
                        </div>
                        <span style={{ color: grey600, marginLeft: 15 }}>od </span>&nbsp;<b>@{notification.author.username}</b>
                    </div>
                </CardTitle>
                <div style={{ height: 3, backgroundColor: priorities[notification.priority].color }}></div>
            </div >
        );
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}

export default NotificationHit;