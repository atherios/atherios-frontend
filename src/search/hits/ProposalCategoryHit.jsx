import React, { Component } from 'react';
import { white, blue300, cyan300, cyan200 } from 'material-ui/styles/colors';

class ProposalCategoryHit extends Component {
    render() {
        const proposalCategory = this.props.payload
        return (
            <div style={{ backgroundColor: cyan300, padding: 20 }}>
                <span style={{ color: white, fontSize: '2em' }}>{proposalCategory.summary}</span>
            </div>
        );
    }
}

export default ProposalCategoryHit;