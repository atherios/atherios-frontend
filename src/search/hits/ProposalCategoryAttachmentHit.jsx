import React, { Component } from 'react';
import InsertDriveFile from 'material-ui/svg-icons/editor/insert-drive-file';
import './../../proposal-categories/settings/attachments/proposal-category-attachment-card.css'
import { blue500 } from 'material-ui/styles/colors';


const fileIconSize = 70;
const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]
class ProposalCategoryAttachmentHit extends Component {
    render() {
        const filename = this.props.payload.file_name;
        return (
            <div className="row">
                <div className="attachment-card__banner" style={{ width: 80, justifyContent: 'center' }}>
                    <br />
                    <div className="attachment-card__icon">
                        <InsertDriveFile style={{ width: fileIconSize, height: fileIconSize }} color={blue500} />
                    </div>
                    <br />
                </div>
                <div style={{ padding: 15, minWidth: 300 }}>
                    <div className="row space--between">
                        <span className="attachment-card__filename">{filename}</span>
                        <span style={{marginLeft: 40}}>{this.computeSize()}</span>
                    </div>
                </div>
            </div>
        );
    }

    computeSize = () => {
        let idx = 0;
        const size = this.props.payload.size;

        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }
}

export default ProposalCategoryAttachmentHit;