import React, { Component } from 'react';

import Requests from '../http/Requests';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Cancel from 'material-ui/svg-icons/navigation/cancel';
import Avatar from 'material-ui/Avatar';
import Link from 'react-router-dom/Link'

import './atherios-drawer.css'
import { connect } from 'react-redux';
import RequiredRoles from '../security/RequiredRoles';

@connect(store => store)
class AtheriosDrawer extends Component {

    render() {
        return (
            <Drawer
                width='100%'
                onRequestChange={this.props.onToggleMenu}
                containerClassName="atherios-layout__drawer"
                open={this.props.open}
                docked={true}>
                <div className="atherios-drawer__menu">
                    <div className="atherios-drawer__user-profile">
                        <div className="user-profile__avatar">
                            <Avatar size={80} src={(this.props.user || {}).avatar} />
                        </div>
                        <div className="user-profile__name">
                            <span>
                                @{(this.props.user || {}).username}
                            </span>
                            <FlatButton
                                style={{ color: "#bcbcbc" }}
                                color="#bcbcbc"
                                onClick={this.logOut}
                                label="wyloguj" />
                        </div>
                    </div>
                    <div>
                        <Link to="/dashboard">
                            <MenuItem>Dashboard</MenuItem>
                        </Link>
                        <RequiredRoles anyRoles={['ROLE_HR_WORKER']}>
                            <Link to="/notifications">
                                <MenuItem>Powiadomienia</MenuItem>
                            </Link>
                            <Link to="/recruitment">
                                <MenuItem>Rekrutacje</MenuItem>
                            </Link>
                            <Link to="/contract-templates">
                                <MenuItem>Szablony umów</MenuItem>
                            </Link>
                            <Link to="/candidates">
                                <MenuItem>Kandydaci</MenuItem>
                            </Link>
                        </RequiredRoles >
                        <RequiredRoles anyRoles={['ROLE_HR_WORKER', 'ROLE_USER']}>
                            <Link to="/proposal-categories">
                                <MenuItem>Wnioski pracowników</MenuItem>
                            </Link>
                        </RequiredRoles>
                        <RequiredRoles anyRoles={['ROLE_ADMIN']}>
                            <Link to="/users-management">
                                <MenuItem>Zarządzanie użytkownikami</MenuItem>
                            </Link>
                        </RequiredRoles>
                    </div>
                    <div className="menu__close-button-wrapper mobile--only">
                        <FlatButton
                            className="menu__close-button"
                            onClick={this.props.onToggleMenu}
                            icon={<Cancel />}
                            label="zamknij"
                            fullWidth={true} />
                    </div>
                </div>
            </Drawer>
        );
    }

    logOut = async () => {
        const response = await Requests.logoutRequest();
        if (response.ok)
            location.href = "/"
    }
}

export default AtheriosDrawer;
