import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import { white, grey300, blue200, lightBlue300, lightBlue600, lightBlue100, cyan200 } from 'material-ui/styles/colors';
import Search from 'material-ui/svg-icons/action/search'
import FlatButton from 'material-ui/FlatButton/FlatButton';
import { withRouter } from "react-router-dom";
const undelineStyle = { borderColor: white };

class SearchForm extends Component {

    state = {
        query: '',
        errors: {

        }
    }

    render() {
        return (
            <form onSubmit={this.redirectToSearch} style={{ width: 400, alignItems: 'center' }} className="row" >
                <Search style={{ marginRight: 15, color: white, width: 40, height: 40 }} />
                <TextField
                    inputStyle={{ color: white }}
                    hintStyle={{ color: cyan200 }}
                    errorText={this.state.errors.query}
                    style={{ color: white }}
                    underlineFocusStyle={undelineStyle}
                    underlineStyle={undelineStyle}
                    value={this.state.query}
                    onChange={(e, value) => this.setState({ query: value })}
                    hintText="Przeszukaj zasoby Atherios"
                    fullWidth={true} />
                <FlatButton onClick={this.redirectToSearch} rippleColor={white} labelStyle={{ color: white }} label="Szukaj" style={{ marginLeft: 15 }} />
                <input type="submit" className="hidden" />
            </form>
        );
    }

    redirectToSearch = (e) => {
        e.preventDefault();
        if (this.validate()) {
            this.props.history.push(`/search/${this.state.query}`);
        }
    }

    validate = () => {
        const errors = {};
        if (!this.state.query || this.state.query.trim().length <= 3)
            errors.query = 'Zapytanie musi być dłuższe niż 3 znaki';

        this.setState({ errors })
        return Object.keys(errors).length === 0;
    }
}

export default withRouter(SearchForm);