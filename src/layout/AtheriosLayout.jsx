import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AtheriosDrawer from './AtheriosDrawer';
import AppBar from 'material-ui/AppBar';
import Requests from '../http/Requests';
import Menu from 'material-ui/svg-icons/navigation/menu';
import IconButton from 'material-ui/IconButton';

import './atherios-layout.css';
import { connect } from 'react-redux';
import SearchForm from './SearchForm';
import RequiredRoles from '../security/RequiredRoles';

@connect(store => store)
class AtheriosLayout extends Component {

    state = {
        currentUser: null,
        open: false
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="atherios-layout">
                    <AtheriosDrawer
                        user={this.props.user}
                        open={this.state.open}
                        onToggleMenu={this.toggleMenu} />
                    <div className="atherios-layout__children">
                        <AppBar
                            iconElementLeft={
                                <IconButton onClick={this.toggleMenu} className="mobile--only">
                                    <Menu />
                                </IconButton>
                            }
                            title="Atherios"
                            iconElementRight={
                                <RequiredRoles anyRoles={['ROLE_ADMN', 'ROLE_HR_WORKER']}>
                                    <SearchForm />
                                </RequiredRoles>
                            }
                        />
                        {this.props.children}
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }

    toggleMenu = () => {
        this.setState({ open: !this.state.open })
    }

    async componentDidMount() {
        const user = await Requests.jsonGet('/api/users/current')
        this.props.dispatch({
            type: 'USER_FETCH_COMPLETED',
            payload: user
        })
    }
}

export default AtheriosLayout;