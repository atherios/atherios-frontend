import React, { Component } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import CardTitle from 'material-ui/Card/CardTitle';
import connect from 'react-redux/lib/connect/connect';
import CreateRecruitmentDialog from './CreateRecruitmentDialog';

@connect(state => state)
class RecruitmentListHeader extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div className="content-card__header">
                <div className="content-card__header__fab">
                    <FloatingActionButton onClick={this.onAddRecruitment} mini={true} className="content-card__header__fab__button" secondary={true}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
                <CardTitle title="Rekrutacje" subtitle="Aktualnie prowadzone rekrutacje" />

                <CreateRecruitmentDialog onClose={this.onDialogClose} open={this.state.dialogOpen} />
            </div>
        );
    }

    onAddRecruitment = () => {
        this.setState({ dialogOpen: true })
    }

    onDialogClose = () => {
        this.setState({ dialogOpen: false })
    }
}

export default RecruitmentListHeader;