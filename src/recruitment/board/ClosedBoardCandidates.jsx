import React, { Component } from 'react';
import { List } from 'material-ui/List';
import BoardCandidate from './BoardCandidate';
import DropTarget from 'react-dnd/lib/DropTarget';
import { connect } from 'react-redux';
import RecruitmentBoardService from './RecruitmentBoardService';
import RecruitmentBoardStore from './RecruitmentBoardStore';
import ClosedBoardCandidate from './ClosedBoardCandidate';

const columnTarget = {
    drop(props, monitor) {
        const candidate = monitor.getItem().candidate;
        const candidateState = RecruitmentBoardService.findCandidateState(candidate.id);
        if (!candidateState.closed)
            switchState(monitor, props);
    }
};

const switchState = async (monitor, props) => {
    const candidateItem = monitor.getItem();
    RecruitmentBoardStore.dispatch({
        type: 'TOGGLE_CLOSING_STATE_DIALOG',
        payload: candidateItem.candidate
    })
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}


@DropTarget("candidate", columnTarget, collect)
@connect(s => s)
class ClosedBoardCandidates extends Component {
    render() {
        const { connectDropTarget, isOver } = this.props;
        const candidates = this.closedCandidates();

        return connectDropTarget(
            <div className={`board-column__candidates ${isOver ? 'board-column--highlighted' : ''}`}>
                {
                    candidates
                        .map((recruitedCandidate, i) =>
                            <ClosedBoardCandidate key={i}
                                recruitmentState={recruitedCandidate.recruitmentState}
                                candidate={recruitedCandidate.candidate} />)
                }
            </div>
        );
    }

    closedCandidates = () => {
        const { board } = this.props;

        const candidateSets = this.props.states
            .filter(recruitmentState => recruitmentState.closed)
            .map(recruitmentState => (board[recruitmentState.id] || [])
                .map(candidate => {
                    return { candidate, recruitmentState }
                }));

        const candidates = [].concat.apply([], candidateSets);
        return candidates;
    }
}

export default ClosedBoardCandidates;