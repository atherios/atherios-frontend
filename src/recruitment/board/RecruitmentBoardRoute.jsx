import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import RecruitmentBoardStore from './RecruitmentBoardStore';
import RecruitmentBoardPage from './RecruitmentBoardPage';

class RecruitementBoardRoute extends Component {
    render() {
        return (
            <Provider store={RecruitmentBoardStore}>
                <RecruitmentBoardPage recruitmentId={this.props.match.params.recruitmentId * 1} />
            </Provider>
        );
    }
}

export default RecruitementBoardRoute;