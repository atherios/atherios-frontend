import React, { Component } from 'react';
import './board-column.css'
import Card from 'material-ui/Card/Card';
import AppBar from 'material-ui/AppBar/AppBar';
import IconButton from 'material-ui/IconButton/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class BoardColumn extends Component {


    render() {
        return (<Card className="board-column">
            <AppBar
                onRightIconButtonClick={this.props.onAddItem}
                iconElementRight={this.props.showButton ? <IconButton><Add /></IconButton> : null}
                showMenuIconButton={false}
                titleStyle={{ fontSize: '1em', padding: 0 }}
                title={this.props.columnLabel} />
            {this.props.children}
        </Card>);
    }
}

export default BoardColumn;