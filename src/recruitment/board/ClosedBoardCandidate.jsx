import React, { Component } from 'react'

import ListItem from 'material-ui/List/ListItem';
import DragSource from 'react-dnd/lib/DragSource';
import Card from 'material-ui/Card/Card';
import { CardText, CardMedia, CardTitle } from 'material-ui/Card';
import Chip from 'material-ui/Chip';
import Link from 'react-router-dom/Link';

const knightSource = {
    beginDrag(props) {
        return props;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

@DragSource("candidate", knightSource, collect)
class ClosedBoardCandidate extends Component {
    render() {
        const { connectDragSource, isDragging } = this.props;
        return connectDragSource(<div>
            <Link to={`/candidates/${this.props.candidate.id}`}>
                <Card style={{
                    cursor: 'pointer',
                    opacity: isDragging ? 0.3 : 1
                }}>
                    <CardText>
                        {this.props.candidate.first_name} {this.props.candidate.last_name} (#{this.props.candidate.id})
                </CardText>
                    <div style={{ padding: '10px' }}>
                        <Chip>
                            {this.props.recruitmentState.label}
                        </Chip>
                    </div>
                </Card>
            </Link>
        </div>);
    }
}

export default ClosedBoardCandidate;