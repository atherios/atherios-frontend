import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RecruitmentBoardService from './RecruitmentBoardService';

@connect(s => s)
export default class CloseCandidateDialog extends Component {

    state = {
        selectedState: null
    }

    render() {
        const candidate = this.props.closeCandidateDialog.candidate || {};
        const closingStates = this.props.states.filter(recruitmentState => recruitmentState.closed)
        return (
            <Dialog
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <div>
                    Zamierzasz zakończyć rekrutację kandydata <b>{candidate.first_name} {candidate.last_name}</b>
                </div>
                <br />
                <div>
                    <SelectField
                        hintText="Stan kończący"
                        value={this.state.selectedState || closingStates[0]}
                        fullWidth={true}
                        onChange={this.handleChange}>
                        {
                            closingStates
                                .map((recruitmentState, i) =>
                                    <MenuItem
                                        key={i}
                                        value={recruitmentState}
                                        primaryText={recruitmentState.label} />)
                        }
                    </SelectField>
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton secondary={true} label="Zamknij" onClick={this.props.onClose} />
                    <FlatButton secondary={true} label="Zapisz" onClick={this.finishRecruitment} />
                </div>
            </Dialog>
        )
    }

    handleChange = (e, index, value) => {
        this.setState({
            selectedState: value
        })
    }

    finishRecruitment = async () => {
        const closingStates = this.props.states.filter(recruitmentState => recruitmentState.closed)

        const candidate = this.props.closeCandidateDialog.candidate;
        const recruitment = this.props.recruitment;
        const recruitmentState = this.state.selectedState || closingStates[0];

        if (!recruitmentState.options.REQUIRE_CONTRACT_GENERATION) {
            await RecruitmentBoardService.switchState(recruitment.id, recruitmentState.id, candidate.id);
            RecruitmentBoardService.loadRecruitmentBoard(recruitment.id);
        }
        else {
            this.props.dispatch({
                type: 'TOGGLE_GENERATE_CONTRACT_DIALOG',
                payload: { candidate, recruitmentState }
            })
        }
        this.props.onClose();
    }
}
