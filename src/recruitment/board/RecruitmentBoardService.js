import RecruitmentBoardStore from "./RecruitmentBoardStore";
import Requests from "../../http/Requests";

export default class RecruitmentBoardService {
    static async loadRecruitment(recruitmentId) {
        const recruitment = await Requests.jsonGet(`/api/recruitment/${recruitmentId}`)

        RecruitmentBoardStore.dispatch({
            type: 'RECRUITMENT_FETCHED',
            payload: recruitment
        })
    }

    static async fetchContractTemplates() {
        const templates  = await Requests.jsonGet('/api/contract-templates');
        RecruitmentBoardStore.dispatch({
            type: 'CONTRACT_TEMPLATES_FETCHED',
            payload: templates
        })
    }

    static async loadRecruitmentStates(recruitmentId) {
        const states = await Requests.jsonGet(`/api/recruitment/${recruitmentId}/states`)

        RecruitmentBoardStore.dispatch({
            type: 'RECRUITMENT_STATES_FETCHED',
            payload: states
        })
    }

    static async loadRecruitmentBoard(recruitmentId) {
        const board = await Requests.jsonGet(`/api/recruitment/${recruitmentId}/board`)

        RecruitmentBoardStore.dispatch({
            type: 'RECRUITMENT_BOARD_FETCHED',
            payload: board
        })
    }

    static async switchState(recruitmentId, stateId, candidateId) {
        return Requests.jsonPut(`/api/recruitment/${recruitmentId}/candidates`, {
            state_id: stateId,
            candidate_id: candidateId
        })
    }

    static findCandidateState(candidateId) {
        const state = RecruitmentBoardStore.getState();

        for (const recruitmentState of state.states) {
            const candidates = state.board[recruitmentState.id] || [];
            for (const candidate of candidates) {
                if (candidate.id === candidateId)
                    return recruitmentState;
            }
        }
    }

    static async saveRecruitmentsOrder(recruitmentId, stateIds) {
        const response = await Requests.jsonPut(`/api/recruitment/${recruitmentId}/states`, stateIds)
        RecruitmentBoardService.loadRecruitmentStates(recruitmentId);
    }
}