import React, { Component } from 'react';
import RecruitmentBoardService from './RecruitmentBoardService';
import AtheriosLayout from '../../layout/AtheriosLayout';
import { Card, CardTitle } from 'material-ui/Card';
import CardText from 'material-ui/Card/CardText';
import { connect } from 'react-redux';
import moment from 'moment';
import RecruitmentBoard from './RecruitmentBoard';
import FloatingActionButton from 'material-ui/FloatingActionButton/FloatingActionButton';
import Settings from 'material-ui/svg-icons/action/settings';
import Link from 'react-router-dom/Link';

@connect(store => store)
class RecruitmentBoardPage extends Component {
    render() {
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        <div className="row space--between" style={{ alignItems: 'center' }}>

                            <div>
                                <span style={{ fontSize: '2em', display: 'block' }}>Rekrutacja {this.props.recruitment.summary}</span>
                                <span style={{ fontSize: '.9em', color: '#808080' }}>Od {this.formatDate(this.props.recruitment.start_date)}
                                    {
                                        this.props.recruitment.end_date ?
                                            <span>
                                                &nbsp;do {this.formatDate(this.props.recruitment.end_date)}
                                            </span> :
                                            null
                                    }
                                </span>
                            </div>
                            <div>
                                <Link to={`/recruitment/${this.props.recruitment.id}/settings`}>
                                    <FloatingActionButton secondary={true}>
                                        <Settings />
                                    </FloatingActionButton>
                                </Link>
                            </div>
                        </div>
                    </CardTitle>
                    {/* <BoardToolbar /> */}
                    <CardText>
                        <RecruitmentBoard />
                    </CardText>
                </Card>
            </AtheriosLayout>
        );
    }

    formatDate(isoDate) {
        return moment(isoDate).format('DD-MM-YYYY');
    }

    componentWillMount() {
        const { recruitmentId } = this.props;

        RecruitmentBoardService.loadRecruitment(recruitmentId);
        RecruitmentBoardService.loadRecruitmentStates(recruitmentId);
        RecruitmentBoardService.loadRecruitmentBoard(recruitmentId);
    }
}

export default RecruitmentBoardPage;