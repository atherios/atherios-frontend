import React, { Component } from 'react';
import { Card } from 'material-ui/Card';
import { List } from 'material-ui/List'
import ListItem from 'material-ui/List/ListItem';
import BoardCandidates from './BoardCandidates';
import AppBar from 'material-ui/AppBar/AppBar';
import { connect } from 'react-redux'
import Add from 'material-ui/svg-icons/content/add';
import IconButton from 'material-ui/IconButton/IconButton';
import AssignCandidateDialog from './AssignCandidateDialog';
import add from 'material-ui/svg-icons/content/add';
import BoardColumn from './BoardColumn';

@connect(s => s)
class RecruitmentBoardColumn extends Component {
    state = {
        dialogOpen: false
    }

    render() {
        return (<BoardColumn
            showButton={true}
            onAddItem={this.openAssignCandidateDialog}
            columnLabel={this.props.recruitmentState.label}>
            <AssignCandidateDialog
                recruitmentState={this.props.recruitmentState}
                open={this.state.dialogOpen}
                onClose={() => this.setState({ dialogOpen: false })} />
            <BoardCandidates recruitmentState={this.props.recruitmentState} />
        </BoardColumn>);
    }

    openAssignCandidateDialog = () => {
        this.setState({ dialogOpen: true })
    }
}

export default RecruitmentBoardColumn;