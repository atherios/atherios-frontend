import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog/Dialog';
import apiOrigin from 'apiOrigin';

@connect(s => s)
class ConfirmContractGeneratedDialog extends Component {
    render() {
        const contractLink = apiOrigin + this.props.confirmContractGeneratedDialog.link;

        return (
            <Dialog open={this.props.open} onRequestClose={this.onClose}>
                Umowa została wygenerowana i jest dostępna pod <a href={contractLink} download>tym adresem</a>.
            </Dialog>
        );
    }

    onClose = () => {
        this.props.dispatch({
            type: 'TOGGLE_CONFIRM_CONTRACT_GENERATED'
        })
    }
}

export default ConfirmContractGeneratedDialog;