import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar/AppBar';
import { Card } from 'material-ui/Card';
import BoardCandidates from './BoardCandidates';
import ClosedBoardCandidates from './ClosedBoardCandidates';
import CloseCandidateDialog from './CloseCandidateDialog';
import GenerateContractDialog from './GenerateContractDialog';
import ConfirmContractGeneratedDialog from './ConfirmContractGeneratedDialog';

@connect(s => s)
class ClosedStateColumn extends Component {


    render() {
        return (<Card className="board-column">
            <AppBar
                style={{ backgroundColor: '#8BC34A' }}
                showMenuIconButton={false}
                titleStyle={{ fontSize: '1em', padding: 0 }}
                title="Rekrutacja zakończona" />
            <CloseCandidateDialog open={this.props.closeCandidateDialog.open} onClose={this.closeCandidateDialog} />
            <GenerateContractDialog open={this.props.generateContractDialog.open} onClose={this.closeGenerateContractDialog} />
            <ConfirmContractGeneratedDialog open={this.props.confirmContractGeneratedDialog.open} onClose={this.closeConfirmContractGeneratedDialog} />
            <ClosedBoardCandidates />
        </Card>
        );
    }

    closeCandidateDialog = () => {
        this.props.dispatch({
            type: 'TOGGLE_CLOSING_STATE_DIALOG'
        })
    }

    closeGenerateContractDialog = () => {
        this.props.dispatch({
            type: 'TOGGLE_GENERATE_CONTRACT_DIALOG'
        })
    }

}

export default ClosedStateColumn;