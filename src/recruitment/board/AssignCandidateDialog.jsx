import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import AutoComplete from 'material-ui/AutoComplete'
import Requests from '../../http/Requests';
import { connect } from 'react-redux';
import RecruitmentBoardService from './RecruitmentBoardService';


const dataSourceConfig = {
    text: 'full_name',
    value: 'id',
};

@connect(s => s)
class AssignCandidateDialog extends Component {

    state = {
        searchText: '',
        hints: []
    }

    render() {
        return (
            <Dialog
                open={this.props.open} onRequestClose={this.props.onClose}
                title="Przypisywanie kandydata">
                <div>
                    <span>Kandydatowi zostanie przypisany stan rekrutacji <b>{this.props.recruitmentState.label}</b></span>
                </div>
                <div>
                    <AutoComplete
                        dataSourceConfig={dataSourceConfig}
                        hintText="Wpisz imię lub nazwisko kandydata"
                        searchText={this.state.searchText}
                        fullWidth={true}
                        onUpdateInput={this.handleUserInput}
                        filter={AutoComplete.fuzzyFilter}
                        onNewRequest={(item, index) => this.setState({ chosenCandidate: item })}
                        dataSource={this.state.hints}
                        openOnFocus={true}
                    />
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton secondary={true} label="Zamknij" onClick={this.props.onClose} />
                    <FlatButton disabled={!this.state.chosenCandidate} secondary={true} label="Zapisz" onClick={this.assignCandidate} />
                </div>
            </Dialog>
        );
    }

    handleUserInput = (searchText) => {
        this.setState({ searchText })
        if (searchText.length === 3)
            this.loadHints(searchText);
    }

    assignCandidate = async () => {
        const { chosenCandidate } = this.state;
        const { recruitment, recruitmentState } = this.props;

        const requestBody = {
            state_id: recruitmentState.id,
            candidate_id: chosenCandidate.id
        };

        await Requests.jsonPost(`/api/recruitment/${recruitment.id}/candidates`, requestBody);
        this.props.onClose();
        RecruitmentBoardService.loadRecruitmentBoard(recruitment.id);
    }

    loadHints = async (phrase) => {
        const hints = await Requests.jsonGet('/api/candidates/search?phrase=' + phrase);
        this.setState({ hints })
    }
}

export default AssignCandidateDialog;