import React, { Component } from 'react';
import ListItem from 'material-ui/List/ListItem';
import DragSource from 'react-dnd/lib/DragSource';
import Card from 'material-ui/Card/Card';
import { CardText } from 'material-ui/Card';
import Link from 'react-router-dom/Link';

const knightSource = {
    beginDrag(props) {
        return props;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

@DragSource("candidate", knightSource, collect)
class BoardCandidate extends Component {
    render() {
        const { connectDragSource, isDragging } = this.props;
        const candidate = this.props.candidate;

        return connectDragSource(<div>
            <Link to={`/candidates/${candidate.id}`}>
                <Card className="candidate-card" style={{
                    cursor: 'pointer',
                    opacity: isDragging ? 0.3 : 1
                }}>
                    <CardText>
                        {candidate.first_name} {candidate.last_name} (#{candidate.id})
                </CardText>
                </Card>
            </Link>
        </div>);
    }
}

export default BoardCandidate;