import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import TextField from 'material-ui/TextField/TextField';
import DatePicker from 'material-ui/DatePicker/DatePicker';
import Divider from 'material-ui/Divider';
import Requests from '../../http/Requests';
import RecruitmentBoardService from './RecruitmentBoardService';

@connect(s => s)
class GenerateContractDialog extends Component {

    state = {
        representedBy: '',
        place: '',
        date: new Date(),
        otherEmploymentConditions: '',
        periodOfWork: '',
        salary: '',
        salary_string: '',
        errors: {}
    }

    render() {
        const candidate = this.props.generateContractDialog.candidate || {};
        const recruitmentState = this.props.generateContractDialog.recruitmentState || {};
        return (
            <Dialog title={
                <p>Generowanie umowy dla <b>{candidate.first_name} {candidate.last_name}</b></p>
            } open={this.props.open} onRequestClose={this.props.onClose}>
                <div>

                </div>
                <div className="row space--between">
                    <TextField
                        value={this.state.place}
                        fullWidth={true}
                        errorText={this.state.errors.place}
                        onChange={(e, value) => this.setState({ place: value })}
                        hintText="*Miejsce podpisania umowy" />
                    <DatePicker
                        value={this.state.date}
                        errorText={this.state.errors.date}
                        onChange={(e, date) => this.setState({ date })}
                        mode="landscape"
                        hintText="Data podpisania umowy" />
                </div>
                <br />
                <div>
                    <TextField
                        value={this.state.representedBy}
                        fullWidth={true}
                        errorText={this.state.errors.representedBy}
                        onChange={(e, value) => this.setState({ representedBy: value })}
                        hintText="*Firma jest reprezentowana przez (biernik)" />
                    <TextField
                        value={this.state.otherEmploymentConditions}
                        fullWidth={true}
                        multiLine={true}
                        errorText={this.state.errors.otherEmploymentConditions}
                        onChange={(e, value) => this.setState({ otherEmploymentConditions: value })}
                        hintText="Pozostałe warunki zatrudnienia" />
                    <TextField
                        value={this.state.periodOfWork}
                        fullWidth={true}
                        errorText={this.state.errors.periodOfWork}
                        onChange={(e, value) => this.setState({ periodOfWork: value })}
                        hintText="*Okres zatrudnienia" />


                </div>
                <br />
                <div>
                    <TextField
                        value={this.state.salary}
                        fullWidth={true}
                        errorText={this.state.errors.salary}
                        onChange={(e, value) => this.setState({ salary: value })}
                        hintText="*Wynagrodzenie" />
                    <TextField
                        value={this.state.salary_string}
                        fullWidth={true}
                        errorText={this.state.errors.salary_string}
                        onChange={(e, value) => this.setState({ salary_string: value })}
                        hintText="*Wynagrodzenie słownie" />
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton secondary={true} label="Zamknij" onClick={this.props.onClose} />
                    <FlatButton secondary={true} label="Generuj umowę" onClick={this.generateContract} />
                </div>
            </Dialog>
        );
    }

    generateContract = () => {
        if (this.validate()) {

            const contract = {
                represented_by: this.state.representedBy,
                place: this.state.place,
                other_employment_conditions: this.state.otherEmploymentConditions,
                date_of_commencement_of_work: new Date().toISOString(),
                period_of_work: this.state.periodOfWork,
                salary: this.state.salary,
                salary_string: this.state.salary_string,
                date: this.state.date.toISOString()
            }

            this.callApi(contract);
        }
    }

    validate = () => {
        const errors = {
        };
        if (!this.state.representedBy)
            errors.representedBy = 'Reprezentant jest wymagany'
        if (!this.state.place)
            errors.place = 'Miejsce podpisania umowy jest wymagane'
        if (!this.state.periodOfWork)
            errors.periodOfWork = 'Okres pracy jest wymagany'
        if (!this.state.salary)
            errors.salary = 'Wynagrodzenie jest wymagane'
        if (!this.state.salary_string)
            errors.salary_string = 'Wynagrodzenie słownie jest wymagane'

        this.setState({ errors })
        return Object.keys(errors).length === 0
    }

    callApi = async (contract) => {
        const { recruitment, generateContractDialog } = this.props;
        await RecruitmentBoardService.switchState(recruitment.id, generateContractDialog.recruitmentState.id, generateContractDialog.candidate.id);

        const response = await Requests.jsonPost(`/api/contracts/recruitment/${recruitment.id}/candidates/${generateContractDialog.candidate.id}`, contract);
        const link = await response.json();


        this.props.dispatch({
            type: 'TOGGLE_GENERATE_CONTRACT_DIALOG',
        })
        RecruitmentBoardService.loadRecruitmentBoard(recruitment.id);
        this.props.dispatch({
            type: 'TOGGLE_CONFIRM_CONTRACT_GENERATED',
            payload: link.link
        })
        
    }
}

export default GenerateContractDialog;