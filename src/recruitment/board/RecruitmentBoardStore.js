import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    recruitment: {},
    states: [],
    board: {},
    closeCandidateDialog: {
        open: false
    },
    generateContractDialog: {
        open: false,
        candidate: {}
    },
    confirmContractGeneratedDialog: {
        open: false
    },
    contractTemplates: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'RECRUITMENT_FETCHED':
            return Object.assign({}, state, {
                recruitment: action.payload
            })
        case 'RECRUITMENT_STATES_FETCHED':
            return Object.assign({}, state, {
                states: action.payload
            })
        case 'RECRUITMENT_BOARD_FETCHED': {
            return Object.assign({}, state, {
                board: action.payload
            })
        }
        case 'TOGGLE_CLOSING_STATE_DIALOG': {
            return Object.assign({}, state, {
                closeCandidateDialog: {
                    open: !state.closeCandidateDialog.open,
                    candidate: action.payload
                }
            })
        }
        case 'TOGGLE_GENERATE_CONTRACT_DIALOG': {
            return Object.assign({}, state, {
                generateContractDialog: {
                    open: !state.generateContractDialog.open,
                    candidate: (action.payload || {}).candidate,
                    recruitmentState: (action.payload || {}).recruitmentState
                }
            })
        }
        case 'TOGGLE_CONFIRM_CONTRACT_GENERATED': {
            return Object.assign({}, state, {
                confirmContractGeneratedDialog: {
                    open: !state.confirmContractGeneratedDialog.open,
                    link: action.payload
                }
            })
        }
        case 'CONTRACT_TEMPLATES_FETCHED': {
            return Object.assign({}, state, {
                contractTemplates: action.payload
            })
        }
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;