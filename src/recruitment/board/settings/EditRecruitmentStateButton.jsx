import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton/IconButton';
import Create from 'material-ui/svg-icons/content/create';
import EditRecruitmentStateDialog from './EditRecruitmentStateDialog';

class EditRecruitmentStateButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <IconButton onClick={e => this.setState({ dialogOpen: true })}>
                    <Create />
                </IconButton>
                <EditRecruitmentStateDialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })}
                    recruitmentState={this.props.recruitmentState} />
            </div>
        );
    }
}

export default EditRecruitmentStateButton;