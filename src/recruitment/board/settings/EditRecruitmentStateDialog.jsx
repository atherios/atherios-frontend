import React, { Component } from 'react';
import Requests from '../../../http/Requests';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import RecruitmentStateForm from './RecruitmentStateForm';
import Dialog from 'material-ui/Dialog/Dialog';
import connect from 'react-redux/lib/connect/connect';
import RecruitmentBoardService from '../RecruitmentBoardService';

@connect(s => s)
class EditRecruitmentStateDialog extends Component {
    render() {
        return (
            <Dialog title="Edycja stanu rekrutacji" open={this.props.open} onRequestClose={this.props.onClose}>
                <RecruitmentStateForm
                    initialState={this.props.recruitmentState}
                    checkboxLabel="Stan oznacza zakończoną rekrutację"
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (recruitmentState) => {
        const response = await Requests.jsonPut(`/api/recruitmentState/${this.props.recruitmentState.id}`, recruitmentState);

        if (response.ok) {
            RecruitmentBoardService.loadRecruitmentStates(this.props.recruitment.id);
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default EditRecruitmentStateDialog;