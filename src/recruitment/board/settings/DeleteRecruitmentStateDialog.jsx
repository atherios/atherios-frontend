import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import RecruitmentBoardService from '../RecruitmentBoardService';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Requests from '../../../http/Requests';

@connect(s => s)
class DeleteRecruitmentStateDialog extends Component {
    render() {
        return (
            <Dialog
                title="Potwierdzenie"
                paperClassName="add-recruitment-dialog"
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <div>
                    Czy na pewno chcesz skasować stan <b>{this.props.recruitmentState.label}</b>?
                    <p>
                        Operacja usunie wszystkie rekrutacje, które znajują się w tym stanie.
                    </p>
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton label="Zamknij" onClick={this.props.onClose} secondary={true} />
                    <FlatButton label="Kasuj" onClick={this.deleteRecruitmentstate} secondary={true} />
                </div>
            </Dialog>
        );
    }

    deleteRecruitmentstate = async () => {
        const response = await Requests.jsonDelete(`/api/recruitmentState/${this.props.recruitmentState.id}`);
        if (response.ok) {
            RecruitmentBoardService.loadRecruitmentStates(this.props.recruitment.id)

        }
    }
}

export default DeleteRecruitmentStateDialog;