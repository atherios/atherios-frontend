import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import AddRecruitmentStateDialog from './AddRecruitmentStateDialog';

class AddRecruitmentStateButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton onClick={() => this.setState({ dialogOpen: true })} secondary={true} label="Dodaj stan" />
                <AddRecruitmentStateDialog open={this.state.dialogOpen} onClose={() => this.setState({ dialogOpen: false })} />
            </div>
        );
    }
}

export default AddRecruitmentStateButton;