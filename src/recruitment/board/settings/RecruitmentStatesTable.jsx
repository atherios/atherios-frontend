import React, { Component } from 'react';
import { Table, TableHeader, TableRow, TableHeaderColumn, TableRowColumn, TableBody } from 'material-ui/Table';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton/IconButton';
import KeyboardArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import RecruitmentBoardService from '../RecruitmentBoardService';
import EditRecruitmentStateButton from './EditRecruitmentStateButton';
import DeleteRecruitmentStateButton from './DeleteRecruitmentStateButton';

@connect(s => s)
class RecruitmentStatesTable extends Component {
    render() {
        return (
            <Table adjustForCheckbox={false} selectable={false}>
                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Nazwa stanu</TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    displayRowCheckbox={false}
                    deselectOnClickaway={false}>
                    {
                        this.props.recruitmentStates
                            .sort((state1, state2) => state1.ordinal - state2.ordinal)
                            .map((state, i) =>
                                <TableRow selectable={false} key={state.id}>
                                    <TableRowColumn>{state.label}</TableRowColumn>
                                    <TableRowColumn>
                                        {this.props.showArrows ? <div>
                                            <IconButton disabled={i <= 0} onClick={e => this.moveUp(state, i)}>
                                                <KeyboardArrowUp />
                                            </IconButton>
                                            <IconButton disabled={i === this.props.recruitmentStates.length - 1} onClick={e => this.moveDown(state, i)}>
                                                <KeyboardArrowDown />
                                            </IconButton>
                                        </div> : null}
                                    </TableRowColumn>
                                    <TableRowColumn>
                                        <div className="row">
                                            {this.props.editStateButton(state)}
                                            {this.props.deleteStateButton(state)}
                                        </div>
                                    </TableRowColumn>
                                </TableRow>)
                    }
                </TableBody>

            </Table>
        );
    }

    moveUp = (state, index) => {
        this.shiftStates(index, -1);
    }

    moveDown = (state, index) => {
        this.shiftStates(index, 1);
    }

    shiftStates = (index, offset) => {
        const { recruitment, recruitmentStates } = this.props;
        const newStates = recruitmentStates.slice();
        const shift1 = recruitmentStates[index];
        const shift2 = recruitmentStates[index + offset];
        newStates[index] = shift2;
        newStates[index + offset] = shift1;
        const stateIds = newStates.map(state => state.id);

        const closedStateIds = this.props.states.filter(state => state.closed === true).map(state => state.id);

        const reorderedStateIds = stateIds.concat(closedStateIds);

        this.props.onStatesOrderSave(reorderedStateIds)
    }
}

export default RecruitmentStatesTable;