import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton/IconButton';
import Clear from 'material-ui/svg-icons/content/clear';
import DeleteRecruitmentStateDialog from './DeleteRecruitmentStateDialog';

class DeleteRecruitmentStateButton extends Component {
    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <IconButton onClick={e => this.setState({ dialogOpen: true })}>
                    <Clear />
                </IconButton>
                <DeleteRecruitmentStateDialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })}
                    recruitmentState={this.props.recruitmentState}
                />
            </div>
        );
    }
}

export default DeleteRecruitmentStateButton;