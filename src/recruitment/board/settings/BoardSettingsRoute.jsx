import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import RecruitmentBoardStore from '../RecruitmentBoardStore';
import BoardSettingsPage from './BoardSettingsPage';

class BoardSettingsRoute extends Component {
    render() {
        return (
            <Provider store={RecruitmentBoardStore}>
                <BoardSettingsPage recruitmentId={this.props.match.params.recruitmentId * 1} />
            </Provider>
        );
    }
}

export default BoardSettingsRoute;