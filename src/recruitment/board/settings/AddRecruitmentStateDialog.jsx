import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import RecruitmentStateForm from './RecruitmentStateForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import { connect } from 'react-redux';
import RecruitmentBoardService from '../RecruitmentBoardService';
import Requests from '../../../http/Requests';

@connect(s => s)
class AddRecruitmentStateDialog extends Component {
    render() {
        return (
            <Dialog title="Nowy stan rekrutacji" open={this.props.open} onRequestClose={this.props.onClose}>
                <RecruitmentStateForm
                    checkboxLabel="Stan oznacza zakończoną rekrutację"
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (recruitmentState) => {
        const response = await Requests.jsonPost(`/api/recruitment/${this.props.recruitment.id}/states`, recruitmentState);

        if (response.ok) {
            RecruitmentBoardService.loadRecruitmentStates(this.props.recruitment.id);
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default AddRecruitmentStateDialog;