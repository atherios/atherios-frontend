import React, { Component } from 'react';
import Card from 'material-ui/Card/Card';
import { CardTitle, CardText } from 'material-ui/Card';
import { Table, TableBody } from 'material-ui/Table';
import TableHeader from 'material-ui/Table/TableHeader';
import RecruitmentStatesTable from './RecruitmentStatesTable';
import { green200 } from 'material-ui/styles/colors'
import AddRecruitmentStateButton from './AddRecruitmentStateButton';
import { connect } from 'react-redux';
import EditRecruitmentStateButton from './EditRecruitmentStateButton';
import DeleteRecruitmentStateButton from './DeleteRecruitmentStateButton';
import RecruitmentBoardService from '../RecruitmentBoardService';

@connect(state => state)
class RecruitmentStatesCard extends Component {
    render() {
        return (
            <Card>
                <CardText>
                    <AddRecruitmentStateButton />
                </CardText>
                <CardTitle>Stany trwających rekrutacji</CardTitle>
                <CardText>
                    <RecruitmentStatesTable
                        showArrows={true}
                        onStatesOrderSave ={this.saveStatesOrder}
                        editStateButton={(recruitmentState) => <EditRecruitmentStateButton recruitmentState={recruitmentState} />}
                        deleteStateButton={(recruitmentState) => <DeleteRecruitmentStateButton recruitmentState={recruitmentState} />}
                        recruitmentStates={this.props.states.filter(state => state.closed === false)}
                    />
                </CardText>
                <CardTitle>Stany zakończonych rekrutacji</CardTitle>
                <CardText>
                    <RecruitmentStatesTable
                        onStatesOrderSave ={this.saveStatesOrder}
                        editStateButton={(recruitmentState) => <EditRecruitmentStateButton recruitmentState={recruitmentState} />}
                        deleteStateButton={(recruitmentState) => <DeleteRecruitmentStateButton recruitmentState={recruitmentState} />}
                        showArrows={false}
                        recruitmentStates={this.props.states.filter(state => state.closed === true)}
                    />
                </CardText>
            </Card>
        );
    }

    saveStatesOrder = (stateIds) => {
        const recruitment = this.props.recruitment;
        RecruitmentBoardService.saveRecruitmentsOrder(recruitment.id, stateIds);
    }
}

export default RecruitmentStatesCard;