import React, { Component } from 'react';
import RecruitmentBoardService from '../RecruitmentBoardService';
import connect from 'react-redux/lib/connect/connect';
import AtheriosLayout from '../../../layout/AtheriosLayout';
import { Card } from 'material-ui/Card';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Link from 'react-router-dom/Link';
import RecruitmentStatesCard from './RecruitmentStatesCard';

@connect(store => store)
class BoardSettingsPage extends Component {
    render() {
        const { recruitment, recruitmentId } = this.props;
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        <div className="row space--between">
                            <div>
                                Ustawienia rekrutacji {recruitment.summary}
                            </div>
                            <div>
                                <Link to={`/recruitment/${recruitment.id}`}>
                                    <FlatButton secondary={true} label="Powrót do tablicy" />
                                </Link>
                            </div>
                        </div>
                    </CardTitle>
                </Card>
                <br />
                <RecruitmentStatesCard />
            </AtheriosLayout>
        );
    }

    componentWillMount() {
        const { recruitmentId } = this.props;
        RecruitmentBoardService.loadRecruitment(recruitmentId);
        RecruitmentBoardService.loadRecruitmentStates(recruitmentId);
        RecruitmentBoardService.fetchContractTemplates();
    }
}

export default BoardSettingsPage;