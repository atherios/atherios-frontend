import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import I18n from '../../../i18n/I18n';
import Checkbox from 'material-ui/Checkbox'
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField/SelectField';
import MenuItem from 'material-ui/MenuItem/MenuItem';
import { connect } from 'react-redux';

@connect(s => s)
class RecruitmentStateForm extends Component {
    constructor(props) {
        super(props);
        const initialState = this.props.initialState || {};
        this.state = Object.assign({
            label: '',
            selectedContractTemplate: null,
            requiresContractGeneration: !!((initialState.options || {}).REQUIRE_CONTRACT_GENERATION),
            closed: false,
            errors: {},
            options: {}
        }, initialState);
    }

    render() {
        return (
            <form onSubmit={this.submitForm} style={{ width: '90%' }}>
                <div className="row">
                    <TextField
                        ref={this.saveRef}
                        fullWidth={true}
                        value={this.state.label}
                        errorText={this.state.errors.label}
                        onChange={(e, value) => this.setState({ label: value })}
                        hintText="*Nazwa stanu" />
                </div>
                <br />
                <div className="row">
                    <Checkbox
                        label={this.props.checkboxLabel}
                        checked={this.state.closed}
                        onCheck={() => this.setState({ closed: !this.state.closed })}
                    />
                </div>
                <br />
                <Divider />
                <br />
                <div>
                    <Checkbox
                        label="Przejście w ten stan wymaga wygenerowania umowy"
                        checked={this.state.requiresContractGeneration}
                        onCheck={() => this.setState({ requiresContractGeneration: !this.state.requiresContractGeneration })}
                    />
                    <br />
                    {
                        (this.state.requiresContractGeneration) ?
                            <SelectField
                                hintText="Wybierz szablon"
                                value={this.state.selectedContractTemplate || this.findContractTemplateById() || this.props.contractTemplates[0]}
                                fullWidth={true}
                                onChange={this.handleChangeSelectedContractTemplate}>
                                {
                                    this.props.contractTemplates
                                        .map((contractTemplate, i) =>
                                            <MenuItem
                                                key={i}
                                                value={contractTemplate}
                                                primaryText={contractTemplate.file_name} />)
                                }
                            </SelectField>
                            : null
                    }
                </div>
                <br />
                {this.props.actionButtons}

            </form>
        );
    }

    handleChangeSelectedContractTemplate = (e, index, value) => {
        this.setState({
            selectedContractTemplate: value
        })
    }

    saveRef = input => {
        if (input)
            input.input.focus()
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const options = {};

            if (this.state.requiresContractGeneration) {
                options.REQUIRE_CONTRACT_GENERATION = {
                    type: 'REQUIRE_CONTRACT_GENERATION',
                    contract_id: this.state.selectedContractTemplate ? this.state.selectedContractTemplate.id : this.props.contractTemplates[0].id
                }
            }

            const recruitmentState = {
                label: this.state.label,
                closed: this.state.closed,
                options
            }

            this.receiveServerValidation(recruitmentState);
        }
    }

    findContractTemplateById = () => {
        const contractRequirement = this.state.options.REQUIRE_CONTRACT_GENERATION;
        if (!contractRequirement)
            return null;

        return this.props.contractTemplates.find(contractTemplate => contractTemplate.id === contractRequirement.contract_id)
    }

    receiveServerValidation = async (recruitmentState) => {
        const serverValidation = await this.props.onFormSubmitted(recruitmentState);
        const errors = I18n.trObject(serverValidation);
        this.setState({ errors });
    }

    validate = () => {
        const errors = {};
        if (!this.state.label)
            errors.label = 'Nazwa stanu jest wymagana'


        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default RecruitmentStateForm;