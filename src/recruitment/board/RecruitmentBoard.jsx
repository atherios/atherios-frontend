import React, { Component } from 'react';
import { connect } from 'react-redux';
import BoardColumn from './BoardColumn';
import ClosedStateColumn from './ClosedStateColumn';
import HTML5Backend from 'react-dnd-html5-backend';
import DragDropContextProvider from 'react-dnd/lib/DragDropContextProvider';
import RecruitmentBoardService from './RecruitmentBoardService';
import RecruitmentBoardColumn from './RecruitmentBoardColumn';

@connect(store => store)
class RecruitmentBoard extends Component {
    render() {
        return (
            <DragDropContextProvider backend={HTML5Backend}>
                <div className="row" style={{width: '1560px', overflowX: 'scroll'}}>
                    {
                        this.props.states
                            .filter(recruitmentState => !recruitmentState.closed)
                            .map((recruitmentState, i) =>
                                <RecruitmentBoardColumn key={recruitmentState.id}
                                    recruitmentState={recruitmentState} />)
                    }
                    <ClosedStateColumn />
                </div>
            </DragDropContextProvider>
        );
    }
}

export default RecruitmentBoard;