import React, { Component } from 'react';
import { List } from 'material-ui/List';
import BoardCandidate from './BoardCandidate';
import DropTarget from 'react-dnd/lib/DropTarget';
import { connect } from 'react-redux';
import RecruitmentBoardService from './RecruitmentBoardService';
import RecruitmentBoardStore from './RecruitmentBoardStore';

const columnTarget = {
    drop(props, monitor) {
        const candidate = monitor.getItem().candidate;
        const candidateState = RecruitmentBoardService.findCandidateState(candidate.id);

        if (candidateState.id !== props.recruitmentState.id)
            switchState(monitor, props);
    }
};

const switchState = async (monitor, props) => {
    const state = RecruitmentBoardStore.getState();
    const candidateItem = monitor.getItem();
    await RecruitmentBoardService.switchState(state.recruitment.id, props.recruitmentState.id, candidateItem.candidate.id);
    RecruitmentBoardService.loadRecruitmentBoard(state.recruitment.id);
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}


@DropTarget("candidate", columnTarget, collect)
@connect(s => s)
class BoardCandidates extends Component {
    render() {
        const { connectDropTarget, isOver } = this.props;
        return connectDropTarget(
            <div className={`board-column__candidates ${isOver ? 'board-column--highlighted' : ''}`}>
                {(this.props.board[this.props.recruitmentState.id] || [])
                    .map(candidate => <BoardCandidate key={candidate.id} candidate={candidate} />)}
            </div>
        );
    }
}

export default BoardCandidates;