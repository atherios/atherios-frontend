import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import Requests from '../http/Requests';
import RecruitmentCard from './RecruitmentCard';
import RecruitmentListHeader from './RecruitmentListHeader';
import './recruitment-page.css';
import { connect } from "react-redux"
import AtheriosLayout from '../layout/AtheriosLayout';

@connect(store => store)
class RecruitmentPage extends Component {

    render() {
        return (
            <AtheriosLayout>
                <div className="recruitment-appbar"></div>
                <div className="content-card__wrapper">
                    <Card className="content-card">
                        <RecruitmentListHeader />
                        <Divider />
                        <br />
                        <div className="recruitment-cards">
                            {this.props.recruitmentList
                                .map((recruitment, i) =>
                                    <RecruitmentCard key={i} recruitment={recruitment} />)}
                        </div>
                    </Card>
                </div>
            </AtheriosLayout>
        );
    }

    async componentWillMount() {
        const recruitment = await Requests.jsonGet('/api/recruitment');
        this.props.dispatch({
            type: 'RECRUITMENT_LIST_FETCHED',
            payload: recruitment
        })
    }

}

export default RecruitmentPage;
