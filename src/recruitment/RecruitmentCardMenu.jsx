import React, { Component } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import EditRecruitmentDialog from './EditRecruitmentDialog';
import connect from 'react-redux/lib/connect/connect';
import Requests from '../http/Requests';
import DeleteRecruitmentDialog from './DeleteRecruitmentDialog';

@connect(store => store)
class RecruitmentCardMenu extends Component {

    state = {
        editDialogOpen: false,
        deleteDialogOpen: false
    }

    render() {
        return (
            <div>
                <IconMenu onClick={e => e.stopPropagation()} iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}>
                    <MenuItem onClick={() => this.setState({ editDialogOpen: true })} primaryText="Edytuj" />
                    <MenuItem onClick={() => this.setState({ deleteDialogOpen: true })} primaryText="Skasuj" />
                </IconMenu>

                <EditRecruitmentDialog
                    open={this.state.editDialogOpen}
                    recruitment={this.props.recruitment}
                    onClose={() => this.setState({ editDialogOpen: false })}
                />

                <DeleteRecruitmentDialog
                    open={this.state.deleteDialogOpen}
                    recruitment={this.props.recruitment}
                    onClose={() => this.setState({ deleteDialogOpen: false })}
                />


            </div>
        );
    }
}

export default RecruitmentCardMenu;