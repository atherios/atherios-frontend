import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField/TextField';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import DatePicker from 'material-ui/DatePicker/DatePicker';

@connect(store => store)
class RecruitmentForm extends Component {

    constructor(props) {
        super(props);
        if (!props.initialState)
            this.state = {
                summary: '',
                startDate: new Date(),
                endDate: undefined,
                errors: {

                }
            };
        else {
            this.state = {
                summary: this.props.initialState.summary,
                startDate: new Date(this.props.initialState.start_date),
                endDate: this.props.initialState.end_date ? new Date(this.props.initialState.end_date) : undefined,
                errors: {

                }
            };
        }
    }

    render() {
        return (
            <form id="recruitment-form" onSubmit={this.submitForm} className="add-recruitment-dialog__body">

                <TextField
                    ref={this.saveRef}
                    value={this.state.summary}
                    errorText={this.state.errors.summary}
                    onChange={(e, value) => this.setState({ summary: value })}
                    style={{ fontSize: '1.5em' }}
                    fullWidth={true}
                    hintText="Opis (np. nazwa stanowiska)" />
                <br />
                <div className="row space--between">
                    <DatePicker
                        value={this.state.startDate}
                        errorText={this.state.errors.startDate}
                        onChange={(e, date) => this.setState({ startDate: date })}
                        mode="landscape"
                        hintText="Data rozpoczęcia rekrutacji" />
                    <DatePicker
                        value={this.state.endDate}
                        onChange={(e, date) => this.setState({ endDate: date })}
                        mode="landscape"
                        hintText="Data zakończenia rekrutacji" />
                </div>
                <br />

                {this.props.actionButtons}

            </form>
        );
    }

    saveRef = input => {
        if (input)
            input.input.focus()
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const recruitment = {
                summary: this.state.summary,
                start_date: this.state.startDate.toISOString(),
            };
            if (this.state.endDate) {
                recruitment.end_date = this.state.endDate.toISOString();
            }
            this.props.onFormSubmitted(recruitment);
        }
    }

    validate = () => {
        const errors = {};
        if (!this.state.summary)
            errors.summary = 'Opis jest wymagany'
        if (!this.state.startDate)
            errors.summary = 'Data rozpoczęcia jest wymagana'

        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default RecruitmentForm;