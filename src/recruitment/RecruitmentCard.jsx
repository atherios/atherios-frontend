import React, { Component } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import './single-recruitement-card.css'
import Divider from 'material-ui/Divider/Divider';
import Link from 'react-router-dom/Link';
import { withRouter } from "react-router-dom";

import RecruitmentCardMenu from './RecruitmentCardMenu';

const colors = [
    '#DCEDC8',
    '#C5CAE9',
    '#E1BEE7',
    '#FFF9C4',
    '#B2EBF2',
    '#E6EE9C'
]
const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

class RecruitmentCard extends Component {
    render() {
        const { recruitment } = this.props;
        return (
            <div onClick={this.redirectToRecruitment} className="single-recruitment-card single-recruitment-card__summary">
                <div className="single-recruitment-card__header"
                    style={{ backgroundColor: colors[recruitment.id % colors.length] }}>
                    <CardTitle title={recruitment.summary} />
                    <RecruitmentCardMenu recruitment={recruitment} />
                </div>
                <Divider />
                <div className="single-recruitment-card__summary__info">
                    {this.startDateContainer()}
                    {
                        recruitment.end_date ? this.endDateContainer() : null
                    }
                </div>
            </div>
        );
    }

    redirectToRecruitment = () => {
        this.props.history.push(`/recruitment/${this.props.recruitment.id}`);
    }

    startDateContainer() {
        return (
            <div>
                <span style={{ display: 'block' }}>Data rozpoczęcia</span>
                <span style={{ color: '#707070' }}>{this.formatDate(this.props.recruitment.start_date)}</span>
            </div>
        );
    }

    endDateContainer() {
        return (
            <div>
                <span style={{ display: 'block' }}>Data zakończenia</span>
                <span style={{ color: '#707070' }}>{this.formatDate(this.props.recruitment.end_date)}</span>
            </div>
        );
    }

    formatDate(date) {
        const timestamp = new Date(date);
        return timestamp.toLocaleDateString("pl-PL", options)
    }
}

export default withRouter(RecruitmentCard);