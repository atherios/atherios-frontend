import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Dialog from 'material-ui/Dialog/Dialog';
import Requests from '../http/Requests';
import { connect } from 'react-redux';

@connect(store => store)
class DeleteRecruitmentDialog extends Component {
    render() {
        return (
            <Dialog
                title="Potwierdzenie"
                paperClassName="add-recruitment-dialog"
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <div>
                    Czy na pewno chcesz skasowac rekrutację <b>{this.props.recruitment.summary}</b>?
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton label="Zamknij" onClick={this.props.onClose} secondary={true} />
                    <FlatButton label="Kasuj" onClick={this.deleteRecruitment} secondary={true} />
                </div>
            </Dialog>
        );
    }

    deleteRecruitment = async () => {
        const response = await Requests.jsonDelete(`/api/recruitment/${this.props.recruitment.id}`);
        if (response.ok) {
            const recruitmentList = await Requests.jsonGet('/api/recruitment');
            this.props.onClose();
            setTimeout(() => {
                this.props.dispatch({
                    type: 'RECRUITMENT_LIST_FETCHED',
                    payload: recruitmentList
                })
            }, 300)

        }
    }
}

export default DeleteRecruitmentDialog;