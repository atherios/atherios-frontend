import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import RecruitmentForm from './RecruitmentForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Requests from '../http/Requests';
import connect from 'react-redux/lib/connect/connect';

@connect(store => store)
class EditRecruitmentDialog extends Component {
    render() {
        return (
            <Dialog
                title={`Edycja rekrutacji ${this.props.recruitment.summary}`}
                paperClassName="add-recruitment-dialog"
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <RecruitmentForm
                    initialState={this.props.recruitment}
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (recruitment) => {
        const response = await Requests.jsonPut(`/api/recruitment/${this.props.recruitment.id}`, recruitment);
        if (response.ok) {
            this.props.onClose();
            const recruitmentList = await Requests.jsonGet('/api/recruitment');
            this.props.dispatch({
                type: 'RECRUITMENT_LIST_FETCHED',
                payload: recruitmentList
            })
        }
    }
}

export default EditRecruitmentDialog;