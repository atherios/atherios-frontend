import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog'
import './add-recruitment-dialog.css';
import Requests from '../http/Requests';
import { connect } from 'react-redux';
import RecruitmentForm from './RecruitmentForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';

@connect(store => store)
class CreateRecruitmentDialog extends Component {


    render() {
        return (
            <Dialog
                title="Dodawanie nowej rekrutacji"
                paperClassName="add-recruitment-dialog"
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <RecruitmentForm
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (recruitment) => {
        const response = await Requests.jsonPost('/api/recruitment', recruitment);


        if (response.ok) {
            const recruitmentList = await Requests.jsonGet('/api/recruitment');
            this.props.dispatch({
                type: 'RECRUITMENT_LIST_FETCHED',
                payload: recruitmentList
            })
            this.props.onClose();
        }
    }
}

export default CreateRecruitmentDialog;