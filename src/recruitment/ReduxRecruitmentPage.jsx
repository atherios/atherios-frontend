import React, { Component } from 'react';
import RecruitmentPage from './RecruitmentPage';
import Provider from 'react-redux/lib/components/Provider';
import RecruitmentStore from './RecruitmentStore';

class ReduxRecruitmentPage extends Component {
    render() {
        return (
            <Provider store={RecruitmentStore}>
                <RecruitmentPage />
            </Provider>
        );
    }
}

export default ReduxRecruitmentPage;