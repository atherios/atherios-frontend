import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import DatePicker from 'material-ui/DatePicker/DatePicker';
import I18n from '../i18n/I18n';
import moment from 'moment';

class CandidateForm extends Component {
    constructor(props) {
        super(props);
        const initialState = this.props.initialState || {};
        this.state = Object.assign({
            first_name: '',
            last_name: '',
            date_of_birth: undefined,
            email: '',
            phone: '',
            evidence_number: '',
            address: '',
            errors: {
            }
        }, initialState);
        if (this.state.date_of_birth)
            this.state.date_of_birth = new Date(this.state.date_of_birth);

    }

    render() {
        return (
            <form onSubmit={this.submitForm} style={{ width: '90%' }}>
                <div className="row">
                    <TextField
                        value={this.state.first_name}
                        errorText={this.state.errors.first_name}
                        onChange={(e, value) => this.setState({ first_name: value })}
                        hintText="*Imię" />
                    <TextField
                        ref={this.saveRef}
                        fullWidth={true}
                        value={this.state.last_name}
                        errorText={this.state.errors.last_name}
                        onChange={(e, value) => this.setState({ last_name: value })}
                        hintText="*Nazwisko" />
                </div>

                <br />
                <div className="">

                    <TextField
                        value={this.state.email}
                        fullWidth={true}
                        errorText={this.state.errors.email}
                        onChange={(e, value) => this.setState({ email: value })}
                        hintText="*Adres e-mail" />
                </div>
                <div className="row space--between">
                    <DatePicker
                        value={this.state.date_of_birth}
                        errorText={this.state.errors.date_of_birth}
                        onChange={(e, date) => this.setState({ date_of_birth: date })}
                        mode="landscape"
                        hintText="*Data urodzenia" />
                    <TextField
                        value={this.state.phone}
                        fullWidth={true}
                        errorText={this.state.errors.phone}
                        onChange={(e, value) => this.setState({ phone: value })}
                        hintText="*Telefon" />
                </div>
                <div className="row">
                    <TextField
                        value={this.state.address}
                        fullWidth={true}
                        errorText={this.state.errors.address}
                        onChange={(e, value) => this.setState({ address: value })}
                        hintText="*Adres" />
                </div>
                <div className="row">
                    <TextField
                        value={this.state.evidence_number}
                        fullWidth={true}
                        errorText={this.state.errors.evidence_number}
                        onChange={(e, value) => this.setState({ evidence_number: value })}
                        hintText="*Numer ewidencyjny (np. PESEL)" />
                </div>
                <br />
                {this.props.actionButtons}

            </form>
        );
    }

    saveRef = input => {
        if (input)
            input.input.focus()
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const candidate = {
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                date_of_birth: moment(this.state.date_of_birth).format('YYYY-MM-DD'),
                email: this.state.email,
                phone: this.state.phone,
                address: this.state.address,
                evidence_number: this.state.evidence_number
            }

            this.receiveServerValidation(candidate);
        }
    }

    receiveServerValidation = async (candidate) => {
        const serverValidation = await this.props.onFormSubmitted(candidate);
        const errors = I18n.trObject(serverValidation);
        this.setState({ errors });
    }

    validate = () => {
        const errors = {};
        if (!this.state.first_name)
            errors.first_name = 'Imię jest wymagane'
        if (!this.state.last_name)
            errors.last_name = 'Nazwisko jest wymagane'
        if (!this.state.date_of_birth)
            errors.date_of_birth = 'Data urodzenia jest wymagana'
        if (!this.state.email)
            errors.email = 'Adres e-mail jest wymagany'
        if (!this.state.phone)
            errors.phone = 'Numer telefonu jest wymagany'
        if (!this.state.evidence_number)
            errors.evidence_number = 'Numer ewidencyjny jest wymagany'
        if (!this.state.address)
            errors.address = 'Adres jest wymagany'

        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default CandidateForm;