import React, { Component } from 'react';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Link from 'react-router-dom/Link';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';

@connect(store => store)
class CandidatesPagination extends Component {
    render() {
        return (
            <div className="row center-content">
                {this.firstPage()}
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    {this.props.current_page >= 1 ?
                        <Link to={`/candidates/page/${this.props.current_page - 1}`}>
                            <RaisedButton label={this.props.current_page} />
                        </Link> : null}

                    <RaisedButton primary={true} label={this.props.current_page + 1} />

                    {this.props.candidates.total_pages - 1 > this.props.current_page ?
                        <Link to={`/candidates/page/${this.props.current_page + 1}`}>
                            <RaisedButton label={this.props.current_page + 2} />
                        </Link>
                        : null}
                </div>
                {this.lastPage()}
            </div>
        );
    }

    firstPage = () => {
        if (this.props.current_page < 2)
            return <div></div>
        return (<div >
            <Link to='/candidates/page/0'>
                <RaisedButton label="1" />
            </Link>
            {this.dots()}
        </div>);
    }

    lastPage = () => {
        if (this.props.candidates.total_pages - this.props.current_page <= 2)
            return <div></div>;
        return (<div >
            {this.dots()}
            <Link to={`/candidates/page/${this.props.candidates.total_pages - 1}`}>
                <RaisedButton label={this.props.candidates.total_pages} />
            </Link>

        </div>)
    }

    dots() {
        return <span style={{ margin: '0 20px' }}>...</span>;
    }
}

export default CandidatesPagination;