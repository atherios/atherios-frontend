import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import { Provider } from 'react-redux';
import CandidatesPage from './CandidatesPage';
import CandidatesStore from './CandidatesStore';


class CandidatesRoute extends Component {
    render() {
        return (
            <Provider store={CandidatesStore}>
                <CandidatesPage page={this.props.match.params.page * 1} />
            </Provider>
        );
    }
}

export default CandidatesRoute;