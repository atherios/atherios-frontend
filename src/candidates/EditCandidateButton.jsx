import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import { fullWhite } from 'material-ui/styles/colors';
import AddCandidateDialog from './AddCandidateDialog';
import EditCandidateDialog from './EditCandidateDialog';
import { connect } from 'react-redux';

@connect(store => store)
class EditCandidateButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onClick={e => this.setState({ dialogOpen: !this.state.dialogOpen })}
                    labelColor={fullWhite}
                    backgroundColor="#a4c639"
                    label="Edytuj kandydata" />
                <EditCandidateDialog
                    candidate={this.props.candidate}
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })} />
            </div>

        );
    }
}

export default EditCandidateButton;