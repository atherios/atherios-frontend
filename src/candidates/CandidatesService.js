import CandidatesStore from "./CandidatesStore";
import Requests from "../http/Requests";



export default class CandidatesService {
    static async refreshCandidatesList() {
        const state = CandidatesStore.getState();
        const candidates = await Requests.jsonGet(`/api/candidates?page=${state.current_page}&limit=15`);

        // TODO: błąd pobierania?
        CandidatesStore.dispatch({
            type: 'CANDIDATES_FETCHED',
            payload: candidates
        })
    }

    static async deleteCandidate(candidateId) {
        return Requests.jsonDelete(`/api/candidates/${candidateId}`);
    }
}