import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import { Provider } from 'react-redux';
import CandidatesPage from './CandidatesPage';
import CandidatesStore from './CandidatesStore';


class CandidatesIndexRoute extends Component {
    render() {
        return (
            <Provider store={CandidatesStore}>
                <CandidatesPage page={0} />
            </Provider>
        );
    }
}

export default CandidatesIndexRoute;