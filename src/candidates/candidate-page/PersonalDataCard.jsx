import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { Card, CardTitle } from 'material-ui/Card';
import { TableHeader, TableHeaderColumn, Table, TableBody } from 'material-ui/Table';
import TableRow from 'material-ui/Table/TableRow';
import TableRowColumn from 'material-ui/Table/TableRowColumn';
import Divider from 'material-ui/Divider/Divider';

@connect(s => s)
class PersonalDataCard extends Component {
    render() {
        const { candidate } = this.props;
        return (
            <Card>
                <CardTitle title="Dane osobowe" />
                <Divider />
                <Table selectable={false}>
                    <TableBody displayRowCheckbox={false}>
                        {this.dataRow('Adres', candidate.address)}
                        {this.dataRow('Data urodzenia', candidate.date_of_birth)}
                        {this.dataRow('Nr ewidencyjny', candidate.evidence_number)}                        
                    </TableBody>
                </Table>
                <Divider />
            </Card>
        );
    }

    dataRow = (label, value) => (<TableRow>
        <TableRowColumn style={{ width: '20%', fontWeight: 'bold' }}>{label}</TableRowColumn>
        <TableRowColumn>{value}</TableRowColumn>
    </TableRow>)
}

export default PersonalDataCard;