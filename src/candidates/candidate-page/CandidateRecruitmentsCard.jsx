import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { Card, CardTitle } from 'material-ui/Card';
import Requests from '../../http/Requests';
import Divider from 'material-ui/Divider/Divider';
import CardText from 'material-ui/Card/CardText';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton/IconButton';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import { blue500 } from 'material-ui/styles/colors';
import Link from 'react-router-dom/Link';

class CandidateRecruitmentsCard extends Component {
    render() {
        const { candidate } = this.props;

        if (this.props.candidateRecruitment.length === 0)
            return (<div></div>);

        return (
            <Card style={{width: '100%'}}>
                <CardTitle title={this.props.title} />
                {
                    this.props.candidateRecruitment
                        .map(recruitment => this.recruitmentCard(recruitment))
                }
            </Card>
        );
    }

    recruitmentCard = (candidateRecruitment) => {
        const { recruitment, state } = candidateRecruitment;
        return (
            <div key={recruitment.id}>
                <Divider />
                <CardText>
                    <div className="row" style={{ alignContent: 'center', alignItems: 'center' }}>
                        <div>
                            <Link to={`/recruitment/${recruitment.id}`}>
                                <IconButton>
                                    <RemoveRedEye color={blue500} />
                                </IconButton>
                            </Link>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div>
                            <span>Rekrutacja na stanowisko <b>{recruitment.summary}</b></span>
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        <div>
                            <Chip>{state.label}</Chip>
                        </div>
                    </div>
                </CardText>
            </div>
        )
    }
}

export default CandidateRecruitmentsCard;