import React, { Component } from 'react';
import { Card, CardText } from 'material-ui/Card';
import CardTitle from 'material-ui/Card/CardTitle';
import connect from 'react-redux/lib/connect/connect';
import Email from 'material-ui/svg-icons/communication/email';
import Phone from 'material-ui/svg-icons/communication/phone';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider/Divider';
import './personal-data-card.css'

@connect(store => store)
class PersonCard extends Component {
    render() {
        const { candidate } = this.props;
        return (
            <Card>
                <br />
                <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                    <div className="candidate-initials">
                        {(candidate.first_name || 'A').substring(0, 1)}{(candidate.last_name || 'A').substring(0, 1)}
                    </div>
                    <span style={{ fontSize: '2.5em', display: 'block' }}>
                        {candidate.first_name} {candidate.last_name}
                    </span>
                </div>
                <br />
                <Divider />
                <div style={{ display: 'flex', justifyContent: 'center', padding: '10px' }} className="row">
                    {this.emailSegment()}
                    {this.phoneSegment()}
                </div>
                <Divider />
            </Card>
        );
    }

    emailSegment = () => {
        const { candidate } = this.props;

        return (<div className="candidate__contact-information">
            <div className="contact-information__icon">
                <Email />
            </div>
            <div className="contact-information__data">
                <span>
                    {candidate.email}
                </span>
            </div>
        </div>);
    }

    phoneSegment = () => {
        const { candidate } = this.props;

        return (<div className="candidate__contact-information">
            <div className="contact-information__icon">
                <Phone />
            </div>
            <div className="contact-information__data">
                <span>
                    {candidate.phone}
                </span>
            </div>
        </div>);
    }
}

export default PersonCard;