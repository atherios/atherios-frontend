import React, { Component } from 'react';
import AtheriosLayout from '../../layout/AtheriosLayout';
import Requests from '../../http/Requests';
import connect from 'react-redux/lib/connect/connect';
import PersonCard from './PersonCard';
import PersonalDataCard from './PersonalDataCard';
import CandidateRecruitmentsCard from './CandidateRecruitmentsCard';

@connect(s => s)
class CandidatePage extends Component {
    render() {
        return (
            <AtheriosLayout>
                <div style={{padding: 30}}>

                    <div className="row">
                        <PersonCard style={{ width: '40%' }} />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <PersonalDataCard style={{ width: '30%' }} />
                    </div>
                    <div className="row" style={{ padding: '30px 0 ' }}>
                        <CandidateRecruitmentsCard
                            title="Trwające rekrutacje"
                            candidateRecruitment={this.props.candidateRecruitment
                                .filter(recruitment => !recruitment.state.closed)} />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <CandidateRecruitmentsCard
                            title="Zakończone rekrutacje"
                            candidateRecruitment={this.props.candidateRecruitment
                                .filter(recruitment => recruitment.state.closed)} />
                    </div>
                </div>
            </AtheriosLayout>
        );
    }

    componentWillMount() {
        const { candidateId } = this.props;
        this.fetchCandidate(candidateId);
        this.fetchRecruitments(candidateId);
    }

    async fetchCandidate(candidateId) {
        const candidate = await Requests.jsonGet(`/api/candidates/${candidateId}`);
        this.props.dispatch({
            type: 'CANDIDATE_FETCHED',
            payload: candidate
        });
    }

    async fetchRecruitments(candidateId) {
        const recruitment = await Requests.jsonGet(`/api/candidates/${candidateId}/recruitment`);
        this.props.dispatch({
            type: 'CANDIDATE_RECRUITMENTS_FETCHED',
            payload: recruitment
        });
    }
}

export default CandidatePage;