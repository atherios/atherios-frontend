import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    candidate: {},
    candidateRecruitment: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'CANDIDATE_FETCHED':
            return Object.assign({}, state, {
                candidate: action.payload
            });
        case 'CANDIDATE_RECRUITMENTS_FETCHED': {
            return Object.assign({}, state, {
                candidateRecruitment: action.payload
            })
        }
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;