import React, { Component } from 'react';
import AtheriosLayout from '../../layout/AtheriosLayout';
import { Provider } from 'react-redux';
import CandidatePage from './CandidatePage';
import CandidateStore from './CandidateStore';


class CandidateRoute extends Component {
    render() {
        return (
            <Provider store={CandidateStore}>
                <CandidatePage candidateId={this.props.match.params.candidateId * 1} />
            </Provider>
        );
    }
}

export default CandidateRoute;