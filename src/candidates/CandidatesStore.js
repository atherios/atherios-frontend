import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    candidates: {
        content: [],
        total_pages: 1,
        total_elements: 0
    },
    current_page: 0
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'CANDIDATES_FETCHED':
            return Object.assign({}, state, {
                candidates: action.payload
            })
        case 'SET_CURRENT_PAGE':
            return Object.assign({}, state, {
                current_page: action.payload
            })
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;