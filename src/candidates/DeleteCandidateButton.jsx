import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import { red400, fullWhite } from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import CandidatesService from './CandidatesService';

@connect(store => store)
class DeleteCandidateButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onClick={e => this.setState({ dialogOpen: true })}
                    label="Skasuj kandydata"
                    backgroundColor={red400} labelColor={fullWhite} />
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: false })}>
                    <div>
                        Czy na pewno chcesz skasować kandydata <b>{this.props.candidate.first_name} {this.props.candidate.last_name}</b>?
                    </div>
                    <br/>
                    <div className="row space--between">
                        <FlatButton secondary={true} label="Zamknij" onClick={() => this.setState({ dialogOpen: false })} />
                        <FlatButton secondary={true} label="Kasuj" onClick={this.deleteCandidate} />
                    </div>
                </Dialog>
            </div>
        );
    }

    deleteCandidate = async () => {
        this.setState({ dialogOpen: false })
        await CandidatesService.deleteCandidate(this.props.candidate.id);
        CandidatesService.refreshCandidatesList();
    }
}

export default DeleteCandidateButton;