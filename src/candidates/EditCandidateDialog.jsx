import React, { Component } from 'react';
import CandidatesService from './CandidatesService';
import Dialog from 'material-ui/Dialog/Dialog';
import CandidateForm from './CandidateForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import connect from 'react-redux/lib/connect/connect';
import Requests from '../http/Requests';

class EditCandidateDialog extends Component {
    render() {
        return (
            <Dialog
                title="Edycja kandydata"
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <CandidateForm
                    initialState={this.props.candidate}
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (candidate) => {
        const response = await Requests.jsonPut(`/api/candidates/${this.props.candidate.id}`, candidate);

        if (response.ok) {
            CandidatesService.refreshCandidatesList();
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default EditCandidateDialog;