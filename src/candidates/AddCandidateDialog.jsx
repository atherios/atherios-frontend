import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import RecruitmentForm from '../recruitment/RecruitmentForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import CandidateForm from './CandidateForm';
import Requests from '../http/Requests';
import connect from 'react-redux/lib/connect/connect';
import CandidatesService from './CandidatesService';

@connect(store => store)
class AddCandidateDialog extends Component {
    
    render() {
        return (
            <Dialog
                title="Rejestracja nowego kandydata"
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <CandidateForm
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (candidate) => {
        const response = await Requests.jsonPost('/api/candidates', candidate);

        if (response.ok) {
            CandidatesService.refreshCandidatesList();
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
    
}

export default AddCandidateDialog;