import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import { connect } from 'react-redux';
import Card from 'material-ui/Card/Card';
import CardText from 'material-ui/Card/CardText';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import CardTitle from 'material-ui/Card/CardTitle';
import { CardActions } from 'material-ui/Card';
import AddCandidateButton from './AddCandidateButton';
import Requests from '../http/Requests';
import EditCandidateButton from './EditCandidateButton';
import DeleteCandidateButton from './DeleteCandidateButton';
import CandidatesService from './CandidatesService';
import CandidatesPagination from './CandidatesPagination';
import Link from 'react-router-dom/Link';

@connect(store => store)
class CandidatesPage extends Component {

    state = {
        isSelected(candidateId) {
            return false;
        }
    }


    render() {
        const selectedCandidate = this.props.candidates.content.find(candidate => candidate.id === this.state.selectedCandidateId)
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        <span style={{ fontSize: '2em' }}>Zarejestrowani kandydaci</span>
                    </CardTitle>
                    <CardActions>
                        <div className="row">
                            <AddCandidateButton />
                            {selectedCandidate ? <EditCandidateButton candidate={selectedCandidate} /> : null}
                            {selectedCandidate ? <DeleteCandidateButton candidate={selectedCandidate} /> : null}
                        </div>
                    </CardActions>
                    <CardText>
                        {this.props.candidates.content.length > 0 ? this.candidatesTable() : this.fallbackCommunicate()}
                    </CardText>
                </Card>
            </AtheriosLayout>
        );
    }

    candidatesTable = () => {
        return (
            <div>
                <Table onRowSelection={this.onRowSelection}>
                    <TableHeader displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>Imię i nazwisko</TableHeaderColumn>
                            <TableHeaderColumn>Adres e-mail</TableHeaderColumn>
                            <TableHeaderColumn>Telefon</TableHeaderColumn>
                            <TableHeaderColumn>Adres</TableHeaderColumn>
                            <TableHeaderColumn>Numer ewidencyjny</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        deselectOnClickaway={false}>
                        {
                            this.props.candidates.content
                                .map((candidate, i) =>
                                    <TableRow selected={this.state.isSelected(candidate.id)} key={candidate.id}>
                                        <TableRowColumn>
                                            <Link style={{color: '#3F51B5'}} to={`/candidates/${candidate.id}`}>
                                                {candidate.first_name} {candidate.last_name}
                                            </Link>
                                        </TableRowColumn>
                                        <TableRowColumn>{candidate.email}</TableRowColumn>
                                        <TableRowColumn>{candidate.phone}</TableRowColumn>
                                        <TableRowColumn>{candidate.address}</TableRowColumn>
                                        <TableRowColumn>{candidate.evidence_number}</TableRowColumn>
                                    </TableRow>)
                        }
                    </TableBody>
                </Table>
                <div>
                    <CandidatesPagination />
                </div>
            </div>
        );
    }

    fallbackCommunicate = () => {
        return (
            <div>
                <h3>
                    Baza kandydatów jest pusta.
                </h3>
            </div>
        )
    }

    onRowSelection = rowNumbers => {
        const candidateIds = rowNumbers.map(rowNum => this.props.candidates.content[rowNum].id);
        if (rowNumbers === 'all')
            this.setState({ isSelected: candidateId => true });
        else if (rowNumbers === 'none')
            this.setState({ isSelected: candidateId => false, selectedCandidateId: undefined });
        else if (rowNumbers.length > 0)
            this.setState({ isSelected: candidateId => candidateIds[0] === candidateId, selectedCandidateId: candidateIds[0] });
        else
            this.setState({ isSelected: candidateId => false, selectedCandidateId: undefined });
    }

    componentDidUpdate() {
        if (this.props.page != this.props.current_page) {
            this.props.dispatch({
                type: 'SET_CURRENT_PAGE',
                payload: this.props.page
            });
            CandidatesService.refreshCandidatesList();
        }
    }

    componentWillMount() {
        CandidatesService.refreshCandidatesList();
    }
}

export default CandidatesPage;
