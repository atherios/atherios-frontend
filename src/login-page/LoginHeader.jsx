import React, { Component } from 'react';
import './login-header.css';

class LoginHeader extends Component {
    render() {
        return (
            <div className="login-page__header">
                <div>
                    <div className="login-header__image-container">
                        <img
                            src="https://image.flaticon.com/icons/svg/226/226673.svg"
                            width="55"
                            height="55" />
                    </div>
                </div>
                <div style={{
                    marginTop: '10px'
                }}>
                    <span
                        style={{
                            fontSize: '1.2em',
                            fontWeight: 400
                        }}>Witamy w atherios.io</span>
                </div>
            </div>
        );
    }
}

export default LoginHeader;
