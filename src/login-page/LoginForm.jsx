import React, { Component } from 'react'
import {
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    CardTitle,
    CardText
} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import LoginHeader from './LoginHeader';
import LoginError from './LoginError';
import Requests from '../http/Requests';
import './login-form.css';

class LoginForm extends Component {

    state = {
        username: '',
        password: '',
        error: undefined
    }
    render() {
        return (
            <Card className="login-page__login-form">
                <form action="" onSubmit={this.onLogin}>
                    <LoginHeader />
                    <CardText>
                        <LoginError visible={this.state.error} />
                        <TextField
                            ref={input => this.loginField = input}
                            value={this.state.username}
                            floatingLabelFixed={true}
                            onChange={(e, value) => this.setState({ username: value })}
                            floatingLabelText="Nazwa użytkownika lub e-mail"
                            fullWidth={true} />
                        <TextField
                            type='password'
                            className="letter-spacing--large"
                            value={this.state.password}
                            floatingLabelFixed={true}
                            onChange={(e, value) => this.setState({ password: value })}
                            floatingLabelText="Hasło"
                            fullWidth={true} />

                    </CardText>
                    <CardActions className="align--right">
                        <input
                            type="submit"
                            style={{
                                display: 'none'
                            }} />
                        <RaisedButton
                            label="Zaloguj"
                            onClick={this.onLogin}
                            primary={true} />
                    </CardActions>
                </form>
            </Card>
        )
    }

    componentDidMount() {
        this.loginField.input.focus();
    }

    onLogin = async (e) => {
        e.preventDefault();

        let loginResponse = await Requests.loginRequest({
            username: this.state.username,
            password: this.state.password
        })
    
        if (loginResponse.ok)
            location.href = '/dashboard'

        if (!this.state.error)
            setTimeout(() => this.setState({ error: undefined }), 5000)
        this.setState({
            error: !loginResponse.ok
        })

    }

}

export default LoginForm;
