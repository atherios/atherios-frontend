import React, { Component } from 'react';
import Error from 'material-ui/svg-icons/alert/error';
import './login-error.css'

class LoginError extends Component {
    render() {
        return (
            <div className="login-error__wrapper">
                <div className={`login-error__message ${this.props.visible ? 'login-error__message--closed' : ''}`}>
                    <Error />
                    <span>Nieprawidłowa nazwa użytkownika lub hasło</span>
                </div>
            </div>
        );
    }
}

export default LoginError;
