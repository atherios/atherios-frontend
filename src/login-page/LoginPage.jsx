import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './login-page.css';
import LoginForm from './LoginForm';

class LoginPage extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <div className="login-page__background">
                    <div className="login-page__container">
                        <LoginForm />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default LoginPage;
