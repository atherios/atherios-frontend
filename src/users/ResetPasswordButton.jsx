import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import { red400, fullWhite } from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import UsersService from './UsersService';
import TextField from 'material-ui/TextField/TextField';

@connect(store => store)
class ResetPasswordButton extends Component {

    state = {
        dialogOpen: false,
        password: '',
        errors: {}
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onClick={e => this.setState({ dialogOpen: true })}
                    label="Resetuj hasło"
                    backgroundColor={red400} labelColor={fullWhite} />
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: false })} title={`Resetuj hasło dla @${this.props.selectedUser.username}`}>
                    <div>
                        <TextField
                            key={1}
                            fullWidth={true}
                            type='password'
                            className="letter-spacing--large"
                            value={this.state.password}
                            errorText={this.state.errors.password}
                            onChange={(e, value) => this.setState({ password: value })}
                            hintText="*Hasło" />
                    </div>
                    <br />
                    <div className="row space--between">
                        <FlatButton secondary={true} label="Zamknij" onClick={() => this.setState({ dialogOpen: false })} />
                        <FlatButton secondary={true} label="Resetuj" onClick={this.resetPassword} />
                    </div>
                </Dialog>
            </div>
        );
    }

    resetPassword = async () => {
        if (this.validate()) {
            this.setState({ dialogOpen: false })
            await UsersService.resetPassword(this.props.selectedUser.username, this.state.password);
            this.setState({ password: '' })
        }
    }

    validate = () => {
        const errors = {};
        if ((this.state.password || '').trim().length < 6)
            errors.password = "Hasło powinno składać się z co najmniej 6 znaków"

        this.setState({ errors });
        return Object.keys(errors).length === 0;
    }
}

export default ResetPasswordButton;
