import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import DatePicker from 'material-ui/DatePicker/DatePicker';
import I18n from '../i18n/I18n';
import moment from 'moment';
import UserRolesSelect from './UserRolesSelect';
import Checkbox from 'material-ui/Checkbox/Checkbox';
import { red800 } from 'material-ui/styles/colors';

class UserForm extends Component {
    constructor(props) {
        super(props);
        const initialState = this.props.initialState || {};
        this.state = Object.assign({
            username: '',
            first_name: '',
            last_name: '',
            blocked: false,
            evidence_number: '',
            roles: [],
            errors: {
            }
        }, initialState);

        console.log(this.props.initialState)

    }

    render() {
        return (
            <form onSubmit={this.submitForm} style={{ width: '100%' }}>
                {
                    this.state.errors.global ?
                        <p style={{ color: red800, fontWeight: 'bold' }}>{this.state.errors.global}</p>
                        : null
                }
                <div className="row">
                    <TextField
                        disabled={!!this.props.diableUsernameChange}
                        fullWidth={true}
                        value={this.state.username}
                        errorText={this.state.errors.username}
                        onChange={(e, value) => this.setState({ username: value })}
                        hintText="*Nazwa użytkownika" />
                </div>
                <br />
                {!!this.props.showPasswordField ?
                    [
                        <div className="row">
                            <TextField
                                key={1}
                                fullWidth={true}
                                type='password'
                                className="letter-spacing--large"
                                value={this.state.password}
                                errorText={this.state.errors.password}
                                onChange={(e, value) => this.setState({ password: value })}
                                hintText="*Hasło" />
                        </div>,
                        <br />] : null}
                <div className="row space--between">

                    <TextField
                        value={this.state.first_name}
                        fullWidth={true}
                        errorText={this.state.errors.first_name}
                        onChange={(e, value) => this.setState({ first_name: value })}
                        hintText="*Imię" />

                    <TextField
                        value={this.state.last_name}
                        fullWidth={true}
                        errorText={this.state.errors.last_name}
                        onChange={(e, value) => this.setState({ last_name: value })}
                        hintText="*Nazwisko" />
                </div>
                <div className="row">
                    <TextField
                        fullWidth={true}
                        value={this.state.evidence_number}
                        errorText={this.state.errors.evidence_number}
                        onChange={(e, value) => this.setState({ evidence_number: value })}
                        hintText="*Numer ewidencyjny" />
                </div>
                <br />
                <div>
                    <br />
                    <Checkbox
                        label="Użytkownik zablokowany"
                        checked={this.state.blocked}
                        onCheck={() => this.setState({ blocked: !this.state.blocked })}
                    />
                    <br />
                </div>
                <div>
                    <UserRolesSelect
                        errors={this.state.errors.roles}
                        selectedRoles={this.state.roles}
                        onRolesSelected={roles => this.setState({ roles })} />
                </div>
                <br />

                {this.props.actionButtons}

            </form>
        );
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const user = {
                username: this.state.username,
                first_name: this.state.first_name,
                evidence_number: this.state.evidence_number,
                last_name: this.state.last_name,
                roles: this.state.roles,
                blocked: this.state.blocked,
                password: this.state.password
            }

            this.receiveServerValidation(user);
        }
    }

    receiveServerValidation = async (user) => {
        const serverValidation = await this.props.onFormSubmitted(user);
        const error = I18n.trObject({ global: serverValidation.message });
        this.setState({ errors: Object.assign({}, this.state.errors, error) });
    }

    validate = () => {
        const errors = {};
        if (!this.state.first_name)
            errors.first_name = 'Imię jest wymagane'
        if (!this.state.last_name)
            errors.last_name = 'Nazwisko jest wymagane'
        if (!this.state.evidence_number)
            errors.evidence_number = 'Numer ewidencyjny jest wymagany'
        if (!this.state.username || !this.state.username.trim())
            errors.username = 'Nazwa użytkownika jest wymagana'
        if (this.state.roles.length === 0)
            errors.roles = 'Użytkownik musi posiadać przynajmniej jedną rolę'
        if (this.props.showPasswordField && (this.state.password || '').trim().length < 6)
            errors.password = "Hasło powinno składać się z co najmniej 6 znaków"

        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default UserForm;
