import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import { fullWhite } from 'material-ui/styles/colors';
import AddUserDialog from './AddUserDialog';
import EditUserDialog from './EditUserDialog';
import { connect } from 'react-redux';

@connect(store => store)
class EditUserButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onClick={e => this.setState({ dialogOpen: !this.state.dialogOpen })}
                    labelColor={fullWhite}
                    backgroundColor="#a4c639"
                    label="Edytuj użytkownika" />
                <EditUserDialog
                    selectedUser={this.props.selectedUser}
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })} />
            </div>

        );
    }
}

export default EditUserButton;
