import UsersStore from "./UsersStore";
import Requests from "../http/Requests";



export default class UsersService {
    static async refreshUsersList() {
        const users = await Requests.jsonGet(`/api/users`)
        UsersStore.dispatch({
            type: 'USERS_FETCHED',
            payload: users
        })
    }

    static async resetPassword(userId, password) {
        return Requests.jsonPut(`/api/users/${userId}/password?password=${password}`, {});
    }
}
