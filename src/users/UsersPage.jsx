import React, { Component } from 'react';
import AtheriosLayout from '../layout/AtheriosLayout';
import { connect } from 'react-redux';
import Card from 'material-ui/Card/Card';
import CardText from 'material-ui/Card/CardText';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import CardTitle from 'material-ui/Card/CardTitle';
import { CardActions } from 'material-ui/Card';
import AddUserButton from './AddUserButton';
import Requests from '../http/Requests';
import EditUserButton from './EditUserButton';
import ResetPasswordButton from './ResetPasswordButton';
import UsersService from './UsersService';
import Chip from 'material-ui/Chip';

@connect(store => store)
class UsersPage extends Component {

    state = {
        isSelected(userId) {
            return false;
        },
        selectedUserId: null
    }


    render() {
        const selectedUser = this.props.users.find(user => user.username === this.state.selectedUserId)
        return (
            <Card>
                <CardTitle>
                    <span style={{ fontSize: '2em' }}>Zarejestrowani użytkownicy</span>
                </CardTitle>
                <CardActions>
                    <div className="row">
                        <AddUserButton />
                        {selectedUser ? <EditUserButton selectedUser={selectedUser} /> : null}
                        {selectedUser ? <ResetPasswordButton selectedUser={selectedUser} /> : null}
                    </div>
                </CardActions>
                <CardText>
                    {this.props.users.length > 0 ? this.usersTable() : this.fallbackCommunicate()}
                </CardText>
            </Card>
        );
    }

    usersTable = () => {
        return (
            <Table onRowSelection={this.onRowSelection}>
                <TableHeader displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Nazwa uzytkownika</TableHeaderColumn>
                        <TableHeaderColumn>Status</TableHeaderColumn>
                        <TableHeaderColumn>Imię i nazwisko</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    deselectOnClickaway={false}>
                    {
                        this.props.users
                            .map((user, i) =>
                                <TableRow selected={this.state.isSelected(user.username)} key={user.username}>
                                    <TableRowColumn><b>@{user.username}</b></TableRowColumn>
                                    <TableRowColumn>
                                        <Chip>{user.blocked ? 'Zablokowany' : 'Aktywny'}</Chip>
                                    </TableRowColumn>
                                    <TableRowColumn>{user.first_name} {user.last_name}</TableRowColumn>
                                </TableRow>)
                    }
                </TableBody>
            </Table>
        );
    }

    fallbackCommunicate = () => {
        return (
            <div>
                <h3>
                    Baza użytkowników jest pusta.
                </h3>
            </div>
        )
    }

    onRowSelection = rowNumbers => {
        const userIds = rowNumbers.map(rowNum => this.props.users[rowNum].username);
        if (rowNumbers === 'all')
            this.setState({ isSelected: userId => true });
        else if (rowNumbers === 'none')
            this.setState({ isSelected: userId => false, selectedUserId: null });
        else if (rowNumbers.length > 0)
            this.setState({ isSelected: userId => userIds[0] === userId, selectedUserId: userIds[0] });
        else
            this.setState({ isSelected: userId => false, selectedUserId: null });
    }

    componentWillMount() {
        UsersService.refreshUsersList();
    }
}

export default UsersPage;
