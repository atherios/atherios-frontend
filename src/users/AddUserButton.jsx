import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import Add from 'material-ui/svg-icons/content/add';
import AddUserDialog from './AddUserDialog';

class AddUserButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onClick={e => this.setState({ dialogOpen: !this.state.dialogOpen })}
                    icon={<Add />}
                    secondary={true}
                    label="Dodaj użytkownika" />
                <AddUserDialog open={this.state.dialogOpen} onClose={() => this.setState({ dialogOpen: false })} />
            </div>
        );
    }
}

export default AddUserButton;
