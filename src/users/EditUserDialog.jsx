import React, { Component } from 'react';
import UsersService from './UsersService';
import Dialog from 'material-ui/Dialog/Dialog';
import UserForm from './UserForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import connect from 'react-redux/lib/connect/connect';
import Requests from '../http/Requests';

class EditUserDialog extends Component {
    render() {
        return (
            <Dialog
                title={`Edycja użytkownika @${this.props.selectedUser.username}`}
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <UserForm
                    diableUsernameChange={true}
                    initialState={this.props.selectedUser}
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (user) => {
        const response = await Requests.jsonPut(`/api/users/${this.props.selectedUser.username}`, user);

        if (response.ok) {
            UsersService.refreshUsersList();
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default EditUserDialog;
