import React, { Component } from 'react';
import { Provider } from 'react-redux';
import UsersPage from './UsersPage';
import UsersStore from './UsersStore';
import AtheriosLayout from '../layout/AtheriosLayout';


class UsersIndexRoute extends Component {
    render() {
        return (
            <Provider store={UsersStore}>
                <AtheriosLayout>
                    <div style={{ padding: 20 }}>

                        <UsersPage />
                    </div>
                </AtheriosLayout>
            </Provider>
        );
    }
}

export default UsersIndexRoute;
