import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField/SelectField';
import { MenuItem } from 'material-ui/Menu';
import Chip from 'material-ui/Chip/Chip';

const POSSIBLE_ROLES = [
    {
        title: 'Administrator',
        value: 'ROLE_ADMIN'
    },
    {
        title: 'Pracownik HR',
        value: 'ROLE_HR_WORKER'
    },
    {
        title: 'Użytkownik',
        value: 'ROLE_USER'
    }
];

const _ROLES = {
    'ROLE_ADMIN': 'Administrator',
    ROLE_HR_WORKER: 'Pracownik HR',
    ROLE_USER: 'Użytkownik'
};

class UserRolesSelect extends Component {

    render() {
        return (
            <div>
                <SelectField
                    hintText="Wybierz role"
                    fullWidth={true}
                    errorText={this.props.errors}
                    onChange={(e, i, value) => this.addRole(value)}>
                    {POSSIBLE_ROLES.map(role =>
                        <MenuItem
                            key={role.value}
                            value={role.value}
                            primaryText={role.title} />)}

                </SelectField>

                <div className="row" >
                    {
                        this.props.selectedRoles.map(role =>
                            <Chip
                                key={role}
                                onRequestDelete={() => this.removeRole(role)}
                                style={{ margin: 5 }}>
                                {_ROLES[role]}
                            </Chip>)
                    }
                </div>
            </div>
        )
    }

    addRole = (item, index) => {
        if (this.props.selectedRoles.some(role => role === item))
            return;

        const selectedUsers = this.props.selectedRoles.concat([item]);
        this.props.onRolesSelected(selectedUsers);

    }

    removeRole = (selectedRole) => {
        const selectedRoles = this.props.selectedRoles
            .filter(role => role !== selectedRole);
        this.props.onRolesSelected(selectedRoles);
    }
}

export default UserRolesSelect;