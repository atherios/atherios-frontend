import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    users: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'USERS_FETCHED':
            return Object.assign({}, state, {
                users: action.payload
            });
    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;
