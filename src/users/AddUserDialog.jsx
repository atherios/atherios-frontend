import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import RecruitmentForm from '../recruitment/RecruitmentForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import UserForm from './UserForm';
import Requests from '../http/Requests';
import connect from 'react-redux/lib/connect/connect';
import UsersService from './UsersService';

@connect(store => store)
class AddUserDialog extends Component {

    render() {
        return (
            <Dialog
                title="Rejestracja nowego użytkownika"
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <UserForm
                    showPasswordField={true}
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (user) => {

        const userRegistration = {
            password: user.password,
            user: user
        }

        const response = await Requests.jsonPost('/api/users', userRegistration);

        if (response.ok) {
            UsersService.refreshUsersList();
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }

}

export default AddUserDialog;
