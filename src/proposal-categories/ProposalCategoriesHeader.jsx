import React, { Component } from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import CardTitle from 'material-ui/Card/CardTitle';
import connect from 'react-redux/lib/connect/connect';
import CreateProposalCategoryDialog from './CreateProposalCategoryDialog';
import RequiredRoles from '../security/RequiredRoles';

class ProposalCategoriesListHeader extends Component {
    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div className="content-card__header">
                <RequiredRoles anyRoles={['ROLE_ADMN', 'ROLE_HR_WORKER']}>
                    <div className="content-card__header__fab">
                        <FloatingActionButton onClick={this.onAddProposalCategory} mini={true} className="content-card__header__fab__button" secondary={true}>
                            <ContentAdd />
                        </FloatingActionButton>
                    </div>
                </RequiredRoles>
                <CardTitle title="Wnioski pracowników" subtitle="Obsługiwane kategorie wniosków" />

                <CreateProposalCategoryDialog onClose={this.onDialogClose} open={this.state.dialogOpen} />
            </div>
        );
    }

    onAddProposalCategory = () => {
        this.setState({ dialogOpen: true })
    }

    onDialogClose = () => {
        this.setState({ dialogOpen: false })
    }
}

export default ProposalCategoriesListHeader;


