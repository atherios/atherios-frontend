import React, { Component } from 'react';
import { IconMenu } from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import EditProposalCategoryDialog from './EditProposalCategoryDialog';

class ProposalCategoryCardMenu extends Component {
    state = {
        editDialogOpen: false,
        deleteDialogOpen: false
    }

    render() {
        return (
            <div>
                <IconMenu onClick={e => e.stopPropagation()} iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}>
                    <MenuItem onClick={() => this.setState({ editDialogOpen: true })} primaryText="Edytuj" />
                    <MenuItem onClick={() => this.setState({ deleteDialogOpen: true })} primaryText="Skasuj" />
                </IconMenu>
                <EditProposalCategoryDialog
                    open={this.state.editDialogOpen}
                    proposalCategory={this.props.proposalCategory}
                    onClose={() => this.setState({ editDialogOpen: false })}
                />
                {/* 
                

                <DeleteRecruitmentDialog
                    open={this.state.deleteDialogOpen}
                    recruitment={this.props.recruitment}
                    onClose={() => this.setState({ deleteDialogOpen: false })}
                /> */}


            </div>
        );
    }
}

export default ProposalCategoryCardMenu;