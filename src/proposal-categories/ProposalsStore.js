import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';

const initialState = {
    user: {},
    proposalCategories: [],
    proposalCategory: {},
    states: [],
    attachments: [],
    board: {},
    closingStateDialog: {
        open: false,
        proposal: {}
    },
    proposal: {},
    proposalHistory: []
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'USER_FETCH_COMPLETED':
            return Object.assign({}, state, {
                user: action.payload
            });
        case 'PROPOSAL_CATEGORIES_FETCHED':
            return Object.assign({}, state, {
                proposalCategories: action.payload
            })
        case 'PROPOSAL_CATEGORY_FETCHED':
            return Object.assign({}, state, {
                proposalCategory: action.payload
            })
        case 'PROPOSAL_CATEGORY_STATES_FETCHED':
            return Object.assign({}, state, {
                states: action.payload
            })
        case 'PROPOSAL_CATEGORY_ATTACHMENTS_FETCHED': {
            return Object.assign({}, state, {
                attachments: action.payload
            })
        }
        case 'PROPOSAL_CATEGORY_BOARD_FETCHED': {
            return Object.assign({}, state, {
                board: action.payload
            })
        }
        case 'TOGGLE_CLOSING_STATE_DIALOG': {
            return Object.assign({}, state, {
                closingStateDialog: {
                    open: !state.closingStateDialog.open,
                    proposal: action.payload
                }
            })
        }
        case 'PROPOSAL_FETCHED': {
            return Object.assign({}, state, {
                proposal: action.payload
            })
        }
        case 'PROPOSAL_HISTORY_FETCHED': {
            return Object.assign({}, state, {
                proposalHistory: action.payload
            })
        }

    }
    return state;
}

const store = createStore(reducer, applyMiddleware(logger));

export default store;