import React, { Component } from 'react';
import Card from 'material-ui/Card/Card';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import './proposal-category-card.css'
import Divider from 'material-ui/Divider/Divider';
import Link from 'react-router-dom/Link';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import FlatButton from 'material-ui/FlatButton/FlatButton';
import ProposalCategoryCardMenu from './ProposalCategoryCardMenu';
import RequiredRoles from './../security/RequiredRoles';


const colors = [
    '#DCEDC8',
    '#C5CAE9',
    '#E1BEE7',
    '#FFF9C4',
    '#B2EBF2',
    '#E6EE9C'
]

@connect(store => store)
class ProposalCategoryCard extends Component {
    render() {
        const proposalCategory = this.props.category;
        return (
            <Card onClick={this.redirectToBoard} className="proposal-category-card">
                <div style={{ backgroundColor: colors[proposalCategory.id % colors.length] }} className="row space--between proposal-category-card__header">
                    <CardTitle title={proposalCategory.summary} /> 
                    <RequiredRoles anyRoles={['ROLE_HR_WORKER']}>
                        <ProposalCategoryCardMenu proposalCategory={proposalCategory} />
                    </RequiredRoles>
                </div>
                <Divider />
                <div className="proposal-category__stats column center-content">
                    <div className="stats__number">{proposalCategory.active_proposals}</div>
                    <div className="stats__label">liczba wniosków do rozpatrzenia</div>
                </div>
                <Divider />
                <div>
                    <FlatButton onClick={this.redirectToProposalUpdate} backgroundColor="#f0f0f0" label="Złóż wniosek" fullWidth={true} />
                </div>
            </Card>

        );
    }

    redirectToBoard = () => {
        if ((this.props.user.roles || []).indexOf('ROLE_HR_WORKER') !== -1)
            this.props.history.push(`/proposal-categories/${this.props.category.id}/board`);
    }

    redirectToProposalUpdate = e => {
        e.stopPropagation();
        this.props.history.push(`/proposal-categories/${this.props.category.id}/proposal-upload`)
    }
}

export default withRouter(ProposalCategoryCard);