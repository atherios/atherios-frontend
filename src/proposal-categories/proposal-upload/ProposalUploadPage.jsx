import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import ProposalsStore from '../ProposalsStore';
import ProposalUpload from './ProposalUpload';

class ProposalUploadPage extends Component {
    render() {
        return (
            <Provider store={ProposalsStore}>
                <ProposalUpload proposalCategoryId={this.props.match.params.proposalCategoryId * 1} />
            </Provider>
        );
    }
}

export default ProposalUploadPage;