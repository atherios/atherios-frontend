import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import Dialog from 'material-ui/Dialog/Dialog';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Link from 'react-router-dom/Link';
import { connect } from 'react-redux';
import ProposalsService from '../ProposalsService';
import Requests from '../../http/Requests';
@connect(s => s)
export default class AddProposalButton extends Component {

    state = {
        dialogOpen: false,
        chosenFile: '',
        confirmationOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton label="Dodaj wniosek" secondary={true} onClick={this.openDialog} />
                <Dialog
                    title="Wybierz plik"
                    open={this.state.dialogOpen}
                    onRequestClose={() => this.setState({ dialogOpen: false })}>
                    <div>
                        <p>
                            Wgrany przez Ciebie plik będzie reprezentował w systemie złożony wniosek.
                        </p>
                    </div>
                    <div className="row">
                        <label ref="file-upload-label" htmlFor="file-upload-input">
                            <RaisedButton onClick={() => this.refs['file-upload-label'].click()} label="Dodaj plik" secondary={true} />
                            <input onChange={this.updateFileName} id="file-upload-input" className="hidden" name="file-upload" type="file" />
                        </label>
                        {this.state.chosenFile ? <div style={{ display: 'flex', alignItems: 'center', paddingLeft: 15 }}>
                            <span>Wybrany plik: <b>{this.state.chosenFile}</b></span>
                        </div> : null}
                    </div>
                    <br />
                    <div className="row space--between">
                        <FlatButton label="Zamknij" secondary={true} onClick={() => this.setState({ dialogOpen: false })} />
                        <FlatButton onClick={this.postProposal} disabled={!this.state.chosenFile} label="Złóż wniosek" secondary={true} />
                    </div>
                </Dialog>
                <Dialog open={this.state.confirmationOpen} onRequestClose={() => this.setState({ confirmationOpen: false })}>
                    <div>
                        Twój wniosek został wysłany. Możesz obejrzeć jego status <Link to="/dashboard">tutaj</Link>.
                    </div>
                </Dialog>
            </div>
        )
    }
    openDialog = () => {
        this.setState({
            dialogOpen: true,
            chosenFile: ''
        });
    }

    updateFileName = (e) => {
        this.setState({
            chosenFile: e.target.files[0].name
        })
    }

    postProposal = async (e) => {
        const file = document.getElementById('file-upload-input').files[0];
        const reader = new FileReader();

        const form_data = new FormData();
        form_data.append('file', file);

        const response = await Requests.postFile('/api/files', form_data);
        const fileJson = await response.json();

        const { proposalCategory, states } = this.props;

        const attachmentResponse = await ProposalsService.createProposal(proposalCategory.id, states[0].id, [fileJson.file_id]);
        this.setState({
            dialogOpen: false,
            confirmationOpen: true
        })
    }
}
