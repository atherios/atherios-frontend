import React, { Component } from 'react'
import { connect } from 'react-redux';
import ProposalsService from '../ProposalsService';
import AtheriosLayout from '../../layout/AtheriosLayout';
import { Card, CardTitle } from 'material-ui/Card';
import Divider from 'material-ui/Divider/Divider';
import CardText from 'material-ui/Card/CardText';
import ProposalCategoryAttachmentsCard from '../settings/attachments/ProposalCategoryAttachmentsCard';
import AddProposalButton from './AddProposalButton';
import { red400 } from 'material-ui/styles/colors';

@connect(s => s)
export default class ProposalUpload extends Component {
    render() {
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        Składasz nowy wniosek w kategorii <b>{this.props.proposalCategory.summary}</b>
                    </CardTitle>
                    <Divider />
                    <CardTitle>
                        <p>
                            Tutaj możesz złożyć nowy wniosek, który zostanie rozpatrzony przez innych użytkowników. Wystarczy, że
                            wgrasz plik (pdf, doc, docx), a nasz system zajmie się resztą.
                        </p>
                        <p>
                            Sugerujemy, aby zapoznać się z materiałami udostępnionymi
                            przez osoby administrujące kategorią wniosków.
                        </p>
                        {
                            this.props.states.length > 0 ?
                                <AddProposalButton /> :
                                <p style={{color: red400}}>
                                    Niestety, do tej kategorii nie można jeszcze dodawać wniosków, ponieważ nie została prawidłowo skonfigurowana
                                </p>
                        }
                    </CardTitle>
                </Card>
                <br />
                <ProposalCategoryAttachmentsCard showAddButton={false} showDeleteButton={false} />
            </AtheriosLayout>
        )
    }

    componentWillMount() {
        const { proposalCategoryId } = this.props;

        ProposalsService.fetchProposalCategory(proposalCategoryId);
        ProposalsService.fetchProposalCategoryAttachments(proposalCategoryId);
        ProposalsService.fetchProposalCategoryStates(proposalCategoryId);
    }
}
