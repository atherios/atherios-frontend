import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import ProposalCategoryForm from './ProposalCategoryForm';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import ProposalsService from './ProposalsService';

class EditProposalCategoryDialog extends Component {
    render() {
        const { proposalCategory } = this.props;
        return (
            <Dialog
                open={this.props.open}
                onRequestClose={this.props.onClose}
                title={<p>Edycja kategorii <b>{proposalCategory.summary}</b></p>}
                modal={false}
                open={this.props.open}

                onRequestClose={this.props.onClose}>
                <ProposalCategoryForm
                    initialState={proposalCategory}
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi} />
            </Dialog>
        );
    }


    callApi = async (proposalCategory) => {
        const errors = await ProposalsService.updateProposalCategory(this.props.proposalCategory.id, proposalCategory);
        if (!errors) {
            this.props.onClose();
        }
    }
}

export default EditProposalCategoryDialog;