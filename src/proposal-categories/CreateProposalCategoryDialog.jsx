import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import ProposalCategoryForm from './ProposalCategoryForm';
import ProposalsService from './ProposalsService';
import error from 'material-ui/svg-icons/alert/error';

class CreateProposalCategoryDialog extends Component {
    render() {
        return (
            <Dialog
                open={this.props.open}
                onRequestClose={this.props.onClose}
                title="Dodawanie nowej kategorii wniosków"
                paperClassName="add-recruitment-dialog"
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onClose}>
                <ProposalCategoryForm
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi} />
            </Dialog>
        );
    }


    callApi = async (proposalCategory) => {
        const errors = await ProposalsService.saveProposalCategory(proposalCategory);
        if (!errors) {
            this.props.onClose();
        }
    }
}

export default CreateProposalCategoryDialog;