import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';

class ProposalCategoryForm extends Component {


    constructor(props) {
        super(props);
        if (!props.initialState)
            this.state = {
                summary: '',
                errors: {}
            };
        else {
            this.state = {
                summary: this.props.initialState.summary,
                errors: {}
            };
        }
    }

    render() {
        return (
            <form id="recruitment-form" onSubmit={this.submitForm}>

                <TextField
                    value={this.state.summary}
                    errorText={this.state.errors.summary}
                    onChange={(e, value) => this.setState({ summary: value })}
                    style={{ fontSize: '1.5em' }}
                    fullWidth={true}
                    hintText="Nazwa kategorii" />
                <br />
                <br />

                {this.props.actionButtons}

            </form>
        );
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const proposalCategory = {
                summary: this.state.summary
            };
            
            this.props.onFormSubmitted(proposalCategory);
        }
    }

    validate = () => {
        const errors = {};
        if (!this.state.summary)
            errors.summary = 'Nazwa kategorii jest wymagana'
        
        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default ProposalCategoryForm;