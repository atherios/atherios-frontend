import React, { Component } from 'react'
import { Card } from 'material-ui/Card';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import { connect } from 'react-redux';
import ProposalHistoryItem from './ProposalHistoryItem';

@connect(s => s)
export default class ProposalHistoryCard extends Component {
    render() {
        const { proposal, proposalHistory } = this.props;
        return (
            <Card>
                <Subheader>Historia zmian wniosku</Subheader>
                <Divider />
                {proposalHistory.map(historyItem => [
                    <ProposalHistoryItem historyItem={historyItem} />,
                    <Divider />
                ])}
            </Card>
        )
    }
}
