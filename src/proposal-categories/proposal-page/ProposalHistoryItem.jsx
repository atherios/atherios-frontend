import React, { Component } from 'react'
import { connect } from 'react-redux';
import './profile-history-item.css';
import Chip from 'material-ui/Chip';
import moment from 'moment';
import { grey500 } from 'material-ui/styles/colors';

@connect(s => s)
export default class ProposalHistoryItem extends Component {
    render() {
        const { historyItem } = this.props;
        return (
            <div style={{ padding: 15 }}>
                <div className="row space--between" style={{ alignItems: 'center' }}>
                    <div className="row" style={{ alignItems: 'center', width: '70%' }} >
                        <b>@{historyItem.user.username}</b>
                        <span>&nbsp;zmienił stan wniosku z&nbsp;&nbsp;</span>
                        {this.previousState()}
                        <span>&nbsp;&nbsp;na&nbsp;&nbsp;</span>
                        {this.nextState()}
                    </div>
                    <span style={{color: grey500, fontWeight: 500}}>
                        {this.formatDate(historyItem.action_time)}
                    </span>
                </div>
            </div>
        )
    }

    findState = (stateId) => {
        return this.props.states.find(state => state.id === stateId);
    }

    previousState = () => {
        const { historyItem } = this.props;
        return <Chip>{this.findState(historyItem.previous_state_id).label}</Chip>;
    }

    nextState = () => {
        const { historyItem } = this.props;
        return <Chip>{this.findState(historyItem.next_state_id).label}</Chip>;
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}
