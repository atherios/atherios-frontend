import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Card } from 'material-ui/Card';
import Divider from 'material-ui/Divider/Divider';
import { pink500, white, grey600 } from 'material-ui/styles/colors';
import Avatar from 'material-ui/Avatar/Avatar';
import Subheader from 'material-ui/Subheader/Subheader';
import moment from 'moment';

const subheaderStyles = { color: grey600, fontWeight: 500 };

@connect(s => s)
export default class ProposalCard extends Component {
    render() {
        const { proposal, proposalCategory, states } = this.props;
        return (
            <Card>
                <br />
                <div className="row" style={{ alignItems: 'center', padding: '0 20px' }}>
                    <Avatar
                        color={white}
                        backgroundColor={pink500}
                        style={{ padding: 5 }}
                        size={55}>#{proposal.id}</Avatar>
                    <span style={{ fontSize: '1.2em', display: 'block', margin: '0 20px' }}>
                        Wniosek z kategorii <b>{proposalCategory.summary}</b>
                    </span>
                </div>
                <br />
                <Divider />
                <div style={{ display: 'flex', justifyContent: 'center' }} className="column">

                    <div style={{ padding: 15 }}>
                        <div className="row space--between">
                            <span style={subheaderStyles}>Składający</span>
                            <span>
                                {(proposal.user || {}).first_name} {(proposal.user || {}).last_name}
                            </span>
                        </div>
                    </div>
                    <Divider />
                    <div style={{ padding: 15 }}>
                        <div className="row space--between">
                            <span style={subheaderStyles}>Data złożenia</span>
                            <span>
                                {this.formatDate(proposal.created_date)}
                            </span>
                        </div>
                    </div>
                </div>
            </Card>
        )
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }

}
