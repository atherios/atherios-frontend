import React, { Component } from 'react'
import Provider from 'react-redux/lib/components/Provider';
import ProposalsStore from '../ProposalsStore';
import Proposal from './Proposal';

export default class ProposalPage extends Component {
    render() {
        return (
            <Provider store={ProposalsStore}>
                <Proposal proposalId={this.props.match.params.proposalId * 1} />
            </Provider>
        )
    }
}
