import React, { Component } from 'react'
import AtheriosLayout from '../../layout/AtheriosLayout';
import { connect } from 'react-redux';
import ProposalsService from '../ProposalsService';
import { Card, CardText } from 'material-ui/Card';
import ProposalCard from './ProposalCard';
import ProposalAttachmentsCard from './ProposalAttachmentsCard';
import ProposalHistoryCard from './ProposalHistoryCard';

@connect(s => s)
export default class Proposal extends Component {
    render() {
        return (
            <AtheriosLayout>
                <div style={{ padding: 20}}>
                    <div className="row" style={{alignContent: 'center'}}>
                        <div style={{ width: '40%', marginRight: 30 }}>
                            <ProposalCard />
                            <br />
                            <ProposalAttachmentsCard />
                        </div>
                        <div style={{ width: '55%' }}>
                            <ProposalHistoryCard />
                        </div>
                    </div>
                </div>
            </AtheriosLayout>
        )
    }

    async componentWillMount() {
        const { proposalId } = this.props;

        const proposal = await ProposalsService.fetchProposal(proposalId);
        ProposalsService.fetchProposalCategory(proposal.category_id)
        await ProposalsService.fetchProposalCategoryStates(proposal.category_id)
        ProposalsService.fetchProposalHistory(proposal.id);
    }


}
