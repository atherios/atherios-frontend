import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Card } from 'material-ui/Card';
import Subheader from 'material-ui/Subheader/Subheader';
import Divider from 'material-ui/Divider/Divider';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import { green600, grey600 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import apiOrigin from 'apiOrigin';

const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]

@connect(s => s)
export default class ProposalAttachmentsCard extends Component {
    render() {
        const { proposal } = this.props;
        return (
            <Card>
                <Subheader>Załączone pliki</Subheader>
                <Divider />
                {(proposal.files || []).map(file => [<div key={file.id} style={{ padding: 10 }}>
                    <div className="row space--between" style={{ alignItems: 'center' }}>
                        <span>
                            {file.file_name} &nbsp;
                            <span style={{ color: grey600 }}>{this.formatSize(file.size)}</span>
                        </span>
                        <a href={this.fileLink(file)} target="_blank">
                            <IconButton iconStyle={{ color: green600 }}>
                                <FileDownload />
                            </IconButton>
                        </a>
                    </div>
                </div>, <Divider />])}
            </Card>
        )
    }

    fileLink(file) {
        return apiOrigin + file.link;
    }

    formatSize(size) {
        let idx = 0;

        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }
}
