import React, { Component } from 'react';
import { connect } from 'react-redux';
import AtheriosLayout from '../layout/AtheriosLayout';
import Card from 'material-ui/Card/Card';
import ProposalCategoriesHeader from './ProposalCategoriesHeader';
import Divider from 'material-ui/Divider/Divider';
import ProposalCategoryCard from './ProposalCategoryCard';
import './proposal-categories.css'

@connect(s => s)
class ProposalCategories extends Component {
    render() {
        return (
            <AtheriosLayout>
                <div className="recruitment-appbar"></div>
                <div className="content-card__wrapper">
                    <Card className="content-card">
                        <ProposalCategoriesHeader />
                        <Divider />
                        <br />
                        <br />
                        <div className="proposals-cards row">
                            {this.props.proposalCategories
                                .map(proposalCategory => <ProposalCategoryCard key={proposalCategory.id} category={proposalCategory} />)}
                        </div>
                        <br />
                    </Card>
                    <br />
                </div>
            </AtheriosLayout>
        );
    }
}

export default ProposalCategories;