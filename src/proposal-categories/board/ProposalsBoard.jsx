import React, { Component } from 'react';

import HTML5Backend from 'react-dnd-html5-backend';
import DragDropContextProvider from 'react-dnd/lib/DragDropContextProvider';
import BoardColumn from '../../recruitment/board/BoardColumn';
import { connect } from 'react-redux';
import ClosedStateColumn from '../../recruitment/board/ClosedStateColumn';
import ProposalsService from '../ProposalsService';
import ProposalBoardColumn from './ProposalBoardColumn';
import ClosedProposalsColumn from './ClosedProposalsColumn';

@connect(s => s)
class ProposalsBoard extends Component {
    render() {
        return (
            <DragDropContextProvider backend={HTML5Backend}>
                <div className="row" style={{ width: '1560px', overflowX: 'scroll' }}>
                    {
                        this.props.states
                            .filter(proposalCategoryState => !proposalCategoryState.closed)
                            .map((proposalCategoryState, i) =>
                                <ProposalBoardColumn key={proposalCategoryState.id}
                                    proposalCategoryState={proposalCategoryState} />)
                    }
                    <ClosedProposalsColumn />
                </div>
            </DragDropContextProvider>
        );
    }
}

export default ProposalsBoard;