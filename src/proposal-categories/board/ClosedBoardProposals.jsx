import React, { Component } from 'react';
import { List } from 'material-ui/List';
import DropTarget from 'react-dnd/lib/DropTarget';
import { connect } from 'react-redux';
import BoardProposal from './BoardProposal';
import Chip from 'material-ui/Chip/Chip';
import ProposalsService from '../ProposalsService';
import CloseProposalDialog from './CloseProposalDialog';
import ProposalsStore from '../ProposalsStore';

const columnTarget = {
    drop(props, monitor) {
        const { proposal } = monitor.getItem();
        const proposalState = ProposalsService.findProposalState(proposal.id);
        if (!proposalState.closed)
            switchState(proposal, props);
    }
};

const switchState = async (proposal, props) => {
    ProposalsStore.dispatch({
        type: 'TOGGLE_CLOSING_STATE_DIALOG',
        payload: proposal
    })
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}


@DropTarget("candidate", columnTarget, collect)
@connect(s => s)
class ClosedBoardProposals extends Component {
    render() {
        const { connectDropTarget, isOver } = this.props;
        const proposals = this.closedProposals();

        return connectDropTarget(
            <div className={`board-column__candidates ${isOver ? 'board-column--highlighted' : ''}`}>
                {
                    proposals
                        .map((boardProposal, i) =>
                            <BoardProposal key={i}
                                proposal={boardProposal.proposal}>
                                <div style={{ padding: '0 10px 10px 10px' }}>
                                    <Chip>{boardProposal.proposalCategoryState.label}</Chip>
                                </div>
                            </BoardProposal>)
                }
                <CloseProposalDialog />
            </div>
        );
    }

    closedProposals = () => {
        const { board } = this.props;

        const proposals = this.props.states
            .filter(proposalCategoryState => proposalCategoryState.closed)
            .map(proposalCategoryState => (board[proposalCategoryState.id] || [])
                .map(proposal => {
                    return { proposal, proposalCategoryState }
                }));

        return [].concat.apply([], proposals);
    }
}

export default ClosedBoardProposals;