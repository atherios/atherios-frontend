import React, { Component } from 'react';
import Card from 'material-ui/Card/Card';
import AppBar from 'material-ui/AppBar/AppBar';
import ClosedBoardProposals from './ClosedBoardProposals';

class ClosedProposalsColumn extends Component {
    render() {
        return (
            <Card className="board-column">
                <AppBar
                    style={{ backgroundColor: '#8BC34A' }}
                    showMenuIconButton={false}
                    titleStyle={{ fontSize: '1em', padding: 0 }}
                    title="Rozpatrzone wnioski" />
                <ClosedBoardProposals />
            </Card>
        );
    }
}

export default ClosedProposalsColumn;