import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog/Dialog';
import { MenuItem } from 'material-ui/IconMenu';
import SelectField from 'material-ui/SelectField/SelectField';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import ProposalsService from '../ProposalsService';

@connect(s => s)
class CloseProposalDialog extends Component {

    state = {
        selectedState: null
    }

    render() {
        const { closingStateDialog } = this.props;
        const closedStates = this.closedStates();
        return (
            <Dialog open={closingStateDialog.open} onRequestClose={this.closeDialog}>
                <div>
                    Zamierzasz zakończyć rozpatrywanie wniosku <b>#{closingStateDialog.proposal.id}</b>
                </div>
                <br />
                <div>
                    <SelectField
                        hintText="Stan kończący"
                        value={this.state.selectedState || closedStates[0]}
                        fullWidth={true}
                        onChange={this.handleChange}>
                        {
                            closedStates
                                .map((proposalCategoryState, i) =>
                                    <MenuItem
                                        key={i}
                                        value={proposalCategoryState}
                                        primaryText={proposalCategoryState.label} />)
                        }
                    </SelectField>
                </div>
                <br />
                <div className="row space--between">
                    <FlatButton secondary={true} label="Zamknij" onClick={this.closeDialog} />
                    <FlatButton secondary={true} label="Zapisz" onClick={this.finishProposal} />
                </div>
            </Dialog>

        );
    }
    handleChange = (e, index, value) => {
        this.setState({
            selectedState: value
        })
    }

    finishProposal = async () => {
        const { proposal } = this.props.closingStateDialog;
        const { proposalCategory } = this.props;

        const proposalCategoryState = this.state.selectedState || this.closedStates()[0];

        await ProposalsService.switchState(proposal.id, proposalCategoryState.id);
        ProposalsService.fetchProposalCategoryBoard(proposalCategory.id);
        
        this.props.dispatch({
            type: 'TOGGLE_CLOSING_STATE_DIALOG',
            payload: {}
        })
    }

    closeDialog = () => {
        this.props.dispatch({
            type: 'TOGGLE_CLOSING_STATE_DIALOG',
            payload: {}
        })
    }

    closedStates = () => {
        return this.props.states
            .filter(state => state.closed);
    }
}

export default CloseProposalDialog;