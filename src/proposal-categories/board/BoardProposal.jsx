import React, { Component } from 'react';
import DragSource from 'react-dnd/lib/DragSource';
import Card from 'material-ui/Card/Card';
import { CardText } from 'material-ui/Card';
import Link from 'react-router-dom/Link';
import Divider from 'material-ui/Divider/Divider';
import Subheader from 'material-ui/Subheader';
import moment from 'moment';
import Avatar from 'material-ui/Avatar/Avatar';
import { deepOrange300, purple500, grey100, blue700, grey700, grey600, grey400, grey500, pink300, pink400, pink500, white } from 'material-ui/styles/colors';


const knightSource = {
    beginDrag(props) {
        return props;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}
const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]

@DragSource("candidate", knightSource, collect)
class BoardProposal extends Component {
    render() {
        const { connectDragSource, isDragging, proposal } = this.props;

        return connectDragSource(<div>
            <Link to={`/proposals/${proposal.id}`}>
                <Card style={{
                    marginBottom: '3px',
                    cursor: 'pointer',
                    opacity: isDragging ? 0.3 : 1
                }}>
                    <div style={{ padding: '10px' }}>
                        <div className="row space--between">

                            <Avatar
                                color={grey100}
                                backgroundColor={blue700}
                                size={35}
                                style={{ fontSize: '1em' }}>#{proposal.id}</Avatar>
                            <span style={{ color: grey500, fontWeight: 500 }}>{this.formatDate(proposal.created_date)}</span>

                        </div>
                        <br />
                        <div className="row space--between">
                            <span style={{ fontSize: '1.3em' }}>{proposal.user.first_name} {proposal.user.last_name}</span>
                        </div>
                    </div>
                    <Divider />
                    <div style={{ padding: 15 }}>
                        <div className="row space--between" style={{ alignItems: 'center' }}>
                            <span>Załączonych plików</span>
                            <Avatar
                                color={white}
                                backgroundColor={pink500}
                                size={25}>{proposal.files.length}</Avatar>
                        </div>
                    </div>
                    {this.props.children}
                </Card>
            </Link>
        </div>);
    }

    computeSize = (size) => {
        let idx = 0;
        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }

    formatDate(isoDateString) {
        return moment(isoDateString).format('DD-MM-YYYY, HH:mm');
    }
}

export default BoardProposal;