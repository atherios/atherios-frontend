import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import ProposalsStore from '../ProposalsStore';
import ProposalCategoryBoard from './ProposalCategoryBoard';

class ProposalCategoryBoardPage extends Component {
    render() {
        return (
            <Provider store={ProposalsStore}>
                <ProposalCategoryBoard proposalCategoryId={this.props.match.params.proposalCategoryId * 1} />
            </Provider>
        );
    }
}

export default ProposalCategoryBoardPage;