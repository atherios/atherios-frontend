import React, { Component } from 'react';
import AtheriosLayout from '../../layout/AtheriosLayout';
import { connect } from 'react-redux';
import ProposalsService from '../ProposalsService';
import { Card, CardTitle } from 'material-ui/Card';
import FloatingActionButton from 'material-ui/FloatingActionButton/FloatingActionButton';
import Settings from 'material-ui/svg-icons/action/settings';
import CardText from 'material-ui/Card/CardText';
import Link from 'react-router-dom/Link';
import ProposalsBoard from './ProposalsBoard';

@connect(s => s)
class ProposalCategoryBoard extends Component {
    render() {
        const { proposalCategory } = this.props;
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        <div className="row space--between" style={{ alignItems: 'center' }}>
                            <div>
                                <span style={{ fontSize: '2em', display: 'block' }}>Wnioski z kategorii <b>{proposalCategory.summary}</b></span>
                            </div>
                            <div>
                                <Link to={`/proposal-categories/${proposalCategory.id}/settings`}>
                                    <FloatingActionButton secondary={true}>
                                        <Settings />
                                    </FloatingActionButton>
                                </Link>
                            </div>
                        </div>
                    </CardTitle>
                    <CardText>
                        <ProposalsBoard />
                    </CardText>
                </Card>
            </AtheriosLayout>
        );
    }

    async componentWillMount() {
        const { proposalCategoryId } = this.props;
        ProposalsService.fetchProposalCategory(proposalCategoryId);
        await ProposalsService.fetchProposalCategoryStates(proposalCategoryId);
        ProposalsService.fetchProposalCategoryBoard(proposalCategoryId);
    }
}

export default ProposalCategoryBoard;