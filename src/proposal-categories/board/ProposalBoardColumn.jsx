import React, { Component } from 'react';
import { connect } from 'react-redux';
import BoardColumn from '../../recruitment/board/BoardColumn';
import BoardProposals from './BoardProposals';

@connect(s => s)
class ProposalBoardColumn extends Component {
    state = {
        dialogOpen: false
    }

    render() {
        const { proposalCategoryState } = this.props;

        return (<BoardColumn
            showButton={false}
            columnLabel={proposalCategoryState.label}>
            <BoardProposals proposalCategoryState={proposalCategoryState} />
        </BoardColumn>);
    }

    openAssignCandidateDialog = () => {
        this.setState({ dialogOpen: true })
    }
}

export default ProposalBoardColumn;