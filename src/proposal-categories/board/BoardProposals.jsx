import React, { Component } from 'react';
import DropTarget from 'react-dnd/lib/DropTarget';
import { connect } from 'react-redux';
import BoardProposal from './BoardProposal';
import ProposalsService from '../ProposalsService';
import ProposalsStore from '../ProposalsStore';


const columnTarget = {
    drop(props, monitor) {
        const proposal = monitor.getItem().proposal;
        const proposalState = ProposalsService.findProposalState(proposal.id);

        if (proposalState.id !== props.proposalCategoryState.id)
            switchState(monitor, props);
    }
};

const switchState = async (monitor, props) => {
    const { proposalCategoryState } = props;
    const { proposal } = monitor.getItem();

    const { proposalCategory } = ProposalsStore.getState();

    await ProposalsService.switchState(proposal.id, proposalCategoryState.id);
    ProposalsService.fetchProposalCategoryBoard(proposalCategory.id);
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}

@DropTarget("candidate", columnTarget, collect)
@connect(s => s)
class BoardProposals extends Component {
    render() {
        const { connectDropTarget, isOver, proposalCategoryState } = this.props;
        return connectDropTarget(
            <div className={`board-column__candidates ${isOver ? 'board-column--highlighted' : ''}`}>
                {(this.props.board[proposalCategoryState.id] || [])
                    .map(proposal => <BoardProposal key={proposal.id} proposal={proposal} />)}
            </div>
        );
    }
}

export default BoardProposals;