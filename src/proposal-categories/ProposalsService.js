import ProposalsStore from "./ProposalsStore";
import Requests from "../http/Requests";



export default class ProposalsService {

    static async fetchProposalCategories() {
        const proposalCategories = await Requests.jsonGet('/api/proposal-categories');
        ProposalsStore.dispatch({
            type: 'PROPOSAL_CATEGORIES_FETCHED',
            payload: proposalCategories
        });
    }

    static async fetchProposal(proposalId) {
        const proposal = await Requests.jsonGet(`/api/proposals/${proposalId}`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_FETCHED',
            payload: proposal
        })
        return proposal;
    }

    static async fetchProposalHistory(proposalId) {
        const history = await Requests.jsonGet(`/api/proposals/${proposalId}/history`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_HISTORY_FETCHED',
            payload: history
        })
    }

    static async switchState(proposalId, stateId) {
        return Requests.jsonPut(`/api/proposals/${proposalId}`, {
            state_id: stateId,
            file_ids: []
        });
    }

    static findProposalState(proposalId) {
        const state = ProposalsStore.getState();

        for (const proposalCategoryState of state.states) {
            const proposals = state.board[proposalCategoryState.id] || [];
            for (const proposal of proposals) {
                if (proposal.id === proposalId)
                    return proposalCategoryState;
            }
        }
    }

    static async createProposal(categoryId, stateId, fileIds) {
        return Requests.jsonPost(`/api/proposal-categories/${categoryId}/proposals`, {
            state_id: stateId,
            file_ids: fileIds
        });
    }

    static async fetchProposalCategoryBoard(categoryId) {
        const board = await Requests.jsonGet(`/api/proposal-categories/${categoryId}/board`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_CATEGORY_BOARD_FETCHED',
            payload: board
        })
    }

    static async saveStatesOrder(proposalCategoryId, stateIds) {
        const response = await Requests.jsonPut(`/api/proposal-categories/${proposalCategoryId}/states`, stateIds)
        ProposalsService.fetchProposalCategoryStates(proposalCategoryId);
    }

    static async deleteAttachment(attachmentId) {
        return Requests.jsonDelete(`/api/proposal-category-attachments/${attachmentId}`);
    }

    static async fetchProposalCategoryAttachments(proposalCategoryId) {
        const attachments = await Requests.jsonGet(`/api/proposal-categories/${proposalCategoryId}/attachments`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_CATEGORY_ATTACHMENTS_FETCHED',
            payload: attachments
        });
    }

    static async createProposalCategoryAttachment(proposalCategoryId, fileId) {
        return Requests.jsonPost(`/api/proposal-categories/${proposalCategoryId}/attachments`, {
            file_id: fileId
        });
    }

    static async fetchProposalCategoryStates(proposalCategoryId) {
        const states = await Requests.jsonGet(`/api/proposal-categories/${proposalCategoryId}/states`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_CATEGORY_STATES_FETCHED',
            payload: states
        });
    }

    static async fetchProposalCategory(proposalCategoryId) {
        const category = await Requests.jsonGet(`/api/proposal-categories/${proposalCategoryId}`);
        ProposalsStore.dispatch({
            type: 'PROPOSAL_CATEGORY_FETCHED',
            payload: category
        });
    }

    static async saveProposalCategory(proposalCategory) {
        const response = await Requests.jsonPost('/api/proposal-categories', proposalCategory);
        if (response.ok) {
            ProposalsService.fetchProposalCategories();
            return null;
        }
        else
            return response.json();
    }


    static async updateProposalCategory(proposalCategoryId, proposalCategory) {
        const response = await Requests.jsonPut(`/api/proposal-categories/${proposalCategoryId}`, proposalCategory);
        if (response.ok) {
            ProposalsService.fetchProposalCategories();
            return null;
        }
        else
            return response.json();
    }



}