import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import ProposalsStore from './ProposalsStore'
import ProposalsService from './ProposalsService';
import { connect } from 'react-redux';
import ProposalCategories from './ProposalCategories';

class ProposalCategoriesPage extends Component {
    render() {
        return (
            <Provider store={ProposalsStore}>
                <ProposalCategories />
            </Provider>
        );
    }

    componentDidMount() {
        ProposalsService.fetchProposalCategories();
    }
}

export default ProposalCategoriesPage;