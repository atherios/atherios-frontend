import React, { Component } from 'react';
import { Card, CardText, CardTitle } from 'material-ui/Card';
import './proposal-category-attachment-card.css'
import InsertDriveFile from 'material-ui/svg-icons/editor/insert-drive-file';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import { blue500, red600, red300, red500 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import Divider from 'material-ui/Divider/Divider';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Delete from 'material-ui/svg-icons/action/delete';

const fileIconSize = 70;
const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]
class FileCard extends Component {
    render() {
        return (
            <Card className="proposal-category-attachment-card">
                <div className="attachment-card__banner">
                    {
                        this.props.showDeleteButton ? <div style={{ display: 'flex', alignSelf: 'flex-end' }}>
                            <IconButton onClick={this.props.onDelete} iconStyle={{ color: red500 }}>
                                <Delete />
                            </IconButton>
                        </div> : null
                    }

                    <br />
                    <br />
                    <div className="attachment-card__icon">
                        <InsertDriveFile style={{ width: fileIconSize, height: fileIconSize }} color={blue500} />
                    </div>
                    <br />
                    <br />
                </div>
                <div style={{ padding: 15 }}>
                    <div className="row space--between">
                        <span className="attachment-card__filename">{this.props.fileName}</span>
                        <span>{this.computeSize()}</span>
                    </div>
                </div>
                <Divider />

                <a target="_blank" href={this.props.link}>
                    <FlatButton icon={<FileDownload />} color="#ffffff" label="Pobierz" fullWidth={true} />
                </a>
            </Card>
        );
    }

    computeSize = () => {
        let idx = 0;
        const size = this.props.size;

        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }
}

export default FileCard;