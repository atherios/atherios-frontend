import React, { Component } from 'react';
import { Card, CardText, CardTitle } from 'material-ui/Card';
import './proposal-category-attachment-card.css'
import InsertDriveFile from 'material-ui/svg-icons/editor/insert-drive-file';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import { blue500, red600, red300, red500 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import apiOrigin from 'apiOrigin';
import Divider from 'material-ui/Divider/Divider';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Delete from 'material-ui/svg-icons/action/delete';
import Dialog from 'material-ui/Dialog/Dialog';
import { connect } from 'react-redux';
import ProposalsService from '../../ProposalsService';
import FileCard from './FileCard';

const fileIconSize = 70;
const units = [{ divider: 1, unit: 'B' }, { unit: 'kB', divider: 1024 }, { unit: 'MB', divider: 1024 * 1024 }, { unit: 'GB', divider: 1024 * 1024 * 1024 }]

@connect(s => s)
class ProposalCategoryAttachmentCard extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <Dialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: false })} title="Potwierdzenie">
                    <div>
                        Czy na pewno chcesz skasować plik <b>{this.props.attachment.file_name}</b>?
                    </div>
                    <div className="row space--between">
                        <div></div>
                        <FlatButton onClick={this.deleteFile} secondary={true} label="Kasuj" />
                    </div>
                </Dialog>
                <FileCard
                    showDeleteButton={this.props.showDeleteButton}
                    link={apiOrigin + '/api' + this.props.attachment.link}
                    onDelete={() => this.setState({ dialogOpen: true })}
                    fileName={this.props.attachment.file_name}
                    size={this.props.attachment.size} />
            </div>
        );
    }

    computeSize = () => {
        let idx = 0;
        const size = this.props.attachment.size;

        while ((size / units[idx++].divider) > 1024);
        return Math.round(((size / units[idx - 1].divider) * 10)) / 10 + ' ' + units[idx - 1].unit;
    }

    deleteFile = () => {
        this.setState({ dialogOpen: false });
        const { id } = this.props.attachment;
        setTimeout(async () => {
            await ProposalsService.deleteAttachment(id);
            ProposalsService.fetchProposalCategoryAttachments(this.props.proposalCategory.id);
        }, 200);

    }
}

export default ProposalCategoryAttachmentCard;