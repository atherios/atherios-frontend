import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import Dialog from 'material-ui/Dialog/Dialog';
import Requests from '../../../http/Requests';
import ProposalsService from '../../ProposalsService';
import {connect} from 'react-redux';

@connect(s => s)
class AddProposalCategoryAttachmentButton extends Component {

    state = { dialogOpen: false }

    render() {
        return (
            <div>
                <label ref="file-upload-label" htmlFor="file-upload-input">
                    <RaisedButton onClick={() => this.refs['file-upload-label'].click()} label="Dodaj plik" secondary={true} />
                    <input onChange={this.uploadFile} id="file-upload-input" name="file-upload" type="file" style={{ display: 'none' }} />
                </label>
            </div>
        );
    }

    uploadFile = async (e) => {
        const file = e.target.files[0];
        const reader = new FileReader();

        const form_data = new FormData();
        form_data.append('file', file);

        const response = await Requests.postFile('/api/files', form_data);
        const fileJson = await response.json();


        const { proposalCategory } = this.props;

        const attachmentResponse = await ProposalsService.createProposalCategoryAttachment(proposalCategory.id, fileJson.file_id);
        ProposalsService.fetchProposalCategoryAttachments(proposalCategory.id)
    }
}

export default AddProposalCategoryAttachmentButton;