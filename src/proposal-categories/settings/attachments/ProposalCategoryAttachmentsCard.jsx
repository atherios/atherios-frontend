import React, { Component } from 'react';
import Card from 'material-ui/Card/Card';
import { CardText, CardTitle } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import { connect } from 'react-redux';
import AddProposalCategoryAttachmentButton from './AddProposalCategoryAttachmentButton';
import ProposalCategoryAttachmentCard from './ProposalCategoryAttachmentCard';

@connect(s => s)
class ProposalCategoryAttachmentsCard extends Component {


    render() {
        return (
            <Card>
                <CardTitle>Dostępne materiały</CardTitle>
                <Divider />
                <CardText>
                    {
                        this.props.showAddButton ? <AddProposalCategoryAttachmentButton /> : null
                    }
                    <br />
                    <div className="row">
                        {this.props.attachments
                            .map(attachment => <ProposalCategoryAttachmentCard key={attachment.id} showDeleteButton={this.props.showDeleteButton} attachment={attachment} />)}
                    </div>
                </CardText>
            </Card>
        );
    }
}

export default ProposalCategoryAttachmentsCard;