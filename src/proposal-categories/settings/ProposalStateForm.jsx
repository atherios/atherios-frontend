import React, { Component } from 'react';
import TextField from 'material-ui/TextField/TextField';
import Checkbox from 'material-ui/Checkbox';
import I18n from '../../i18n/I18n';

class ProposalStateForm extends Component {
    constructor(props) {
        super(props);
        const initialState = this.props.initialState || {};
        this.state = Object.assign({
            label: '',
            closed: false,
            errors: {}
        }, initialState);
    }

    render() {
        return (
            <form onSubmit={this.submitForm} style={{ width: '90%' }}>
                <div className="row">
                    <TextField
                        ref={this.saveRef}
                        fullWidth={true}
                        value={this.state.label}
                        errorText={this.state.errors.label}
                        onChange={(e, value) => this.setState({ label: value })}
                        hintText="*Nazwa stanu" />
                </div>
                <br/>
                <div className="row">
                    <Checkbox
                        label={this.props.checkboxLabel}
                        checked={this.state.closed}
                        onCheck={() => this.setState({closed: !this.state.closed})}
                    />
                </div>
                <br/>
                {this.props.actionButtons}

            </form>
        );
    }

    saveRef = input => {
        if (input)
            input.input.focus()
    }

    submitForm = (e) => {
        e.preventDefault();
        if (this.validate()) {
            const recruitmentState = {
                label: this.state.label,
                closed: this.state.closed
            }

            this.receiveServerValidation(recruitmentState);
        }
    }

    receiveServerValidation = async (recruitmentState) => {
        const serverValidation = await this.props.onFormSubmitted(recruitmentState);
        const errors = I18n.trObject(serverValidation);
        this.setState({ errors });
    }

    validate = () => {
        const errors = {};
        if (!this.state.label)
            errors.label = 'Nazwa stanu jest wymagana'


        this.setState({ errors })
        return Object.keys(errors).length === 0
    }
}

export default ProposalStateForm;