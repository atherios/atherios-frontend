import React, { Component } from 'react';
import Provider from 'react-redux/lib/components/Provider';
import ProposalsStore from '../ProposalsStore';
import ProposalCategorySettings from './ProposalCategorySettings';

class ProposalCategorySettingsPage extends Component {
    render() {
        return (
            <Provider store={ProposalsStore}>
                <ProposalCategorySettings proposalCategoryId={this.props.match.params.proposalCategoryId * 1}  />
            </Provider>
        );
    }
}

export default ProposalCategorySettingsPage;