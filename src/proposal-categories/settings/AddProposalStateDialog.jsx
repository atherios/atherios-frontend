import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import ProposalsService from '../ProposalsService';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Requests from '../../http/Requests';
import { connect } from 'react-redux';
import ProposalStateForm from './ProposalStateForm';

@connect(s => s)
class AddProposalStateDialog extends Component {
    render() {
        return (
            <Dialog title="Nowy stan wniosków" open={this.props.open} onRequestClose={this.props.onClose}>
                <ProposalStateForm
                    checkboxLabel="Stan oznacza zakończenie rozpatrywania wniosku"
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (proposalState) => {
        const { proposalCategory } = this.props;
        const response = await Requests.jsonPost(`/api/proposal-categories/${proposalCategory.id}/states`, proposalState);

        if (response.ok) {
            ProposalsService.fetchProposalCategoryStates(proposalCategory.id);
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default AddProposalStateDialog;