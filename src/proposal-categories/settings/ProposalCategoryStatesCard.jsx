import React, { Component } from 'react';
import { Card, CardText, CardTitle } from 'material-ui/Card';
import AddRecruitmentStateButton from '../../recruitment/board/settings/AddRecruitmentStateButton';
import RecruitmentStatesTable from '../../recruitment/board/settings/RecruitmentStatesTable';
import EditRecruitmentStateButton from '../../recruitment/board/settings/EditRecruitmentStateButton';
import DeleteRecruitmentStateButton from '../../recruitment/board/settings/DeleteRecruitmentStateButton';
import AddProposalStateButton from './AddProposalStateButton';
import { connect } from 'react-redux';
import EditProposalStateButton from './EditProposalStateButton';
import ProposalsService from '../ProposalsService';

@connect(s => s)
class ProposalCategoryStatesCard extends Component {
    render() {
        const proposalCategoryStates = this.props.states;
        return (
            <Card>
                <CardText>
                    <AddProposalStateButton />
                </CardText>
                <CardTitle>Stany trwających rekrutacji</CardTitle>
                <CardText>
                    <RecruitmentStatesTable
                        onStatesOrderSave={this.saveStatesOrder}
                        editStateButton={(proposalState) => <EditProposalStateButton proposalState={proposalState} />}
                        deleteStateButton={(proposalState) => <DeleteRecruitmentStateButton recruitmentState={proposalState} />}
                        showArrows={true}
                        recruitmentStates={proposalCategoryStates.filter(state => !state.closed)}
                    />
                </CardText>
                <CardTitle>Stany zakończonych rekrutacji</CardTitle>
                <CardText>
                    <RecruitmentStatesTable
                        onStatesOrderSave={this.saveStatesOrder}
                        editStateButton={(proposalState) => <EditProposalStateButton proposalState={proposalState} />}
                        deleteStateButton={(proposalState) => <DeleteRecruitmentStateButton recruitmentState={proposalState} />}
                        showArrows={false}
                        recruitmentStates={proposalCategoryStates.filter(state => state.closed)}
                    />
                </CardText>
            </Card>
        );
    }

    saveStatesOrder = async (stateIds) => {
        const { proposalCategory } = this.props;
        await ProposalsService.saveStatesOrder(proposalCategory.id, stateIds);
        ProposalsService.fetchProposalCategoryStates(proposalCategory.id)
    }
}

export default ProposalCategoryStatesCard;