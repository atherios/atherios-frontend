import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import AddProposalStateDialog from './AddProposalStateDialog';

class AddProposalStateButton extends Component {
    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <RaisedButton onClick={() => this.setState({ dialogOpen: true })} secondary={true} label="Dodaj stan" />
                <AddProposalStateDialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })}
                />
            </div>
        );
    }
}

export default AddProposalStateButton;