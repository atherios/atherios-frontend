import React, { Component } from 'react';
import AtheriosLayout from '../../layout/AtheriosLayout';
import { connect } from 'react-redux';
import ProposalsService from '../ProposalsService';
import { Card, CardTitle } from 'material-ui/Card';
import Link from 'react-router-dom/Link';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import ProposalCategoryStatesCard from './ProposalCategoryStatesCard';
import ProposalCategoryAttachmentsCard from './attachments/ProposalCategoryAttachmentsCard';


@connect(s => s)
class ProposalCategorySettings extends Component {
    render() {
        const { proposalCategory } = this.props;
        return (
            <AtheriosLayout>
                <Card>
                    <CardTitle>
                        <div className="row space--between">
                            <div>
                                Ustawienia kategorii wniosków <b>{proposalCategory.summary}</b>
                            </div>
                            <div>
                                <Link to={`/proposal-categories/${proposalCategory.id}/board`}>
                                    <FlatButton secondary={true} label="Powrót do tablicy" />
                                </Link>
                            </div>
                        </div>
                    </CardTitle>
                </Card>
                <br />
                <ProposalCategoryAttachmentsCard showAddButton={true} showDeleteButton={true} />
                <br/>
                <ProposalCategoryStatesCard />
            </AtheriosLayout>
        );
    }

    componentDidMount() {
        const { proposalCategoryId } = this.props;
        ProposalsService.fetchProposalCategory(proposalCategoryId)
        ProposalsService.fetchProposalCategoryStates(proposalCategoryId)
        ProposalsService.fetchProposalCategoryAttachments(proposalCategoryId)
    }
}

export default ProposalCategorySettings;