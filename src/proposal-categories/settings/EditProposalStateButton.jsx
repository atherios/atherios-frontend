import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton/IconButton';
import Create from 'material-ui/svg-icons/content/create';
import EditProposalStateDialog from './EditProposalStateDialog';

class EditProposalStateButton extends Component {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <div>
                <IconButton onClick={e => this.setState({ dialogOpen: true })}>
                    <Create />
                </IconButton>
                <EditProposalStateDialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({ dialogOpen: false })}
                    proposalState={this.props.proposalState} />
            </div>
        );
    }

}

export default EditProposalStateButton;