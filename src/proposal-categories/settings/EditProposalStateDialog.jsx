import React, { Component } from 'react';
import {connect} from 'react-redux';
import ProposalsService from '../ProposalsService';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import Dialog from 'material-ui/Dialog/Dialog';
import Requests from '../../http/Requests';
import ProposalStateForm from './ProposalStateForm';

@connect(s => s)
class EditProposalStateDialog extends Component {
    render() {
        return (
            <Dialog title="Edycja stanu wnisków" open={this.props.open} onRequestClose={this.props.onClose}>
                <ProposalStateForm
                    initialState={this.props.proposalState}
                    checkboxLabel="Stan oznacza zakończenie rozpatrywania wniosku"
                    actionButtons={<div className="row space--between">
                        <FlatButton onClick={this.props.onClose} label="Zamknij" secondary={true} />
                        <FlatButton
                            label="Zapisz"
                            containerElement="label"
                            secondary={true}>
                            <input type="submit" style={{ display: 'none' }} />
                        </FlatButton>
                    </div>}
                    onFormSubmitted={this.callApi}
                />
            </Dialog>
        );
    }

    callApi = async (proposalState) => {
        const response = await Requests.jsonPut(`/api/proposal-states/${this.props.proposalState.id}`, proposalState);

        if (response.ok) {
            ProposalsService.fetchProposalCategoryStates(this.props.proposalCategory.id);
            this.props.onClose();
        }
        else {
            return response.json();
        }
    }
}

export default EditProposalStateDialog;