import React from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Route
} from 'react-router-dom';
import LoginPage from './login-page/LoginPage';
import UserDashboardPage from './user-dashboard/UserDashboardPage';
import RecruitmentPage from './recruitment/RecruitmentPage';
import ReduxRecruitmentPage from './recruitment/ReduxRecruitmentPage';
import CandidatesRoute from './candidates/CandidatesRoute';
import CandidatesIndexRoute from './candidates/CandidatesIndexRoute';
import UsersIndexRoute from './users/UsersIndexRoute';
import RecruitmentBoardRoute from './recruitment/board/RecruitmentBoardRoute';
import BoardSettingsRoute from './recruitment/board/settings/BoardSettingsRoute';
import CandidateRoute from './candidates/candidate-page/CandidateRoute';
import ProposalCategoriesPage from './proposal-categories/ProposalCategoriesPage';
import ProposalCategoryBoardPage from './proposal-categories/board/ProposalCategoryBoardPage';
import ProposalCategorySettingsPage from './proposal-categories/settings/ProposalCategorySettingsPage';
import ProposalUploadPage from './proposal-categories/proposal-upload/ProposalUploadPage';
import ProposalPage from './proposal-categories/proposal-page/ProposalPage';
import ContractTemplatePage from './contract-templates/ContractTemplatePage';
import NotificationsPage from './notifications/NotificationsPage';
import AddNotificationPage from './notifications/AddNotificationPage';
import EditNotificationPage from './notifications/EditNotificationPage';
import NotificationDistributionPage from './notifications/distribution/NotificationDistributionPage';
import SearchResultsPage from './search/SearchResultsPage';


const Routes = () => (
    <Router>
        <div>
            <Route exact path="/" component={LoginPage} />
            <Route exact path="/dashboard" component={UserDashboardPage} />
            <Route exact path="/search/:query" component={SearchResultsPage} />
            <Route exact path="/recruitment/:recruitmentId(\d+)" component={RecruitmentBoardRoute} />
            <Route exact path="/recruitment/:recruitmentId(\d+)/settings" component={BoardSettingsRoute} />
            <Route exact path="/recruitment" component={ReduxRecruitmentPage} />
            <Route exact path="/candidates/page/:page(\d+)" component={CandidatesRoute} />
            <Route exact path="/candidates" component={CandidatesIndexRoute} />
            <Route exact path="/candidates/:candidateId(\d+)" component={CandidateRoute} />

            <Route exact path="/proposal-categories" component={ProposalCategoriesPage} />
            <Route exact path="/proposal-categories/:proposalCategoryId/board" component={ProposalCategoryBoardPage} />
            <Route exact path="/proposal-categories/:proposalCategoryId/proposal-upload" component={ProposalUploadPage} />
            <Route exact path="/proposal-categories/:proposalCategoryId/settings" component={ProposalCategorySettingsPage} />
            <Route exact path="/proposals/:proposalId" component={ProposalPage} />

            <Route exact path="/contract-templates" component={ContractTemplatePage} />

            <Route exact path="/notifications" component={NotificationsPage} />
            <Route exact path="/notifications/add-notification" component={AddNotificationPage} />
            <Route exact path="/notifications/:notificationId/edit-notification" component={EditNotificationPage} />
            <Route exact path="/notifications/:notificationId/distribution" component={NotificationDistributionPage} />

            <Route exact path="/users-management" component={UsersIndexRoute} />
        </div>
    </Router>
);

export default Routes;
